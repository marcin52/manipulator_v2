/*
 * test_dofControl.c
 *
 *  Created on: 22 gru 2020
 *      Author: marci
 */

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

 //-- unity: unit test framework
#include "unity.h"

//-- module being tested
#include "dofControl.h"

//--mocks
#include "mock_pca9685.h"

//-- external libraries
#include <string.h>
#include <stdio.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define STM32L432xx

#define DOF1_MAX_ANGLE				180
#define DOF2_MAX_ANGLE				270
#define DOF3_MAX_ANGLE				270
#define DOF4_MAX_ANGLE				180
#define DOF5_MAX_ANGLE				180
#define DOF6_MAX_ANGLE				180
#define GRIPPER_MAX_ANGLE			180

#define DOF1_INIT_ANGLE 			90
#define DOF2_INIT_ANGLE 			135
#define DOF3_INIT_ANGLE 			135
#define DOF4_INIT_ANGLE 			60
#define DOF5_INIT_ANGLE 			90
#define DOF6_INIT_ANGLE 			90

/*******************************************************************************
*    PRIVATE TYPES
******************************************************************************/

/*******************************************************************************
*    PRIVATE DATA
******************************************************************************/

/*******************************************************************************
*    PRIVATE FUNCTIONS
******************************************************************************/

/*******************************************************************************
*    SETUP, TEARDOWN
******************************************************************************/

void setUp(void)
{
	PCA9685_Init_IgnoreAndReturn(0);
	PCA9685_SetServoAngle_IgnoreAndReturn(0);
	initDofControl();
}

void tearDown(void)
{

}

/*******************************************************************************
 *    TESTS
 ******************************************************************************/

// Init tests

void test_initAngleDefinitionsMatchWithSource(){

	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE, getDofAngle(dofNumber1));
	TEST_ASSERT_EQUAL(DOF2_INIT_ANGLE, getDofAngle(dofNumber2));
	TEST_ASSERT_EQUAL(DOF3_INIT_ANGLE, getDofAngle(dofNumber3));
	TEST_ASSERT_EQUAL(DOF4_INIT_ANGLE, getDofAngle(dofNumber4));
	TEST_ASSERT_EQUAL(DOF5_INIT_ANGLE, getDofAngle(dofNumber5));
	TEST_ASSERT_EQUAL(DOF6_INIT_ANGLE, getDofAngle(dofNumber6));

}

void test_maxAngleDefinitionsMatchWithSource(){

	TEST_ASSERT_EQUAL(DOF1_MAX_ANGLE, getDofMaxAngle(dofNumber1));
	TEST_ASSERT_EQUAL(DOF2_MAX_ANGLE, getDofMaxAngle(dofNumber2));
	TEST_ASSERT_EQUAL(DOF3_MAX_ANGLE, getDofMaxAngle(dofNumber3));
	TEST_ASSERT_EQUAL(DOF4_MAX_ANGLE, getDofMaxAngle(dofNumber4));
	TEST_ASSERT_EQUAL(DOF5_MAX_ANGLE, getDofMaxAngle(dofNumber5));
	TEST_ASSERT_EQUAL(DOF6_MAX_ANGLE, getDofMaxAngle(dofNumber6));

}

void test_initExpectedAnglesAreInitAngles(){

	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE, getExpectedDofAngle(dofNumber1));
	TEST_ASSERT_EQUAL(DOF2_INIT_ANGLE, getExpectedDofAngle(dofNumber2));
	TEST_ASSERT_EQUAL(DOF3_INIT_ANGLE, getExpectedDofAngle(dofNumber3));
	TEST_ASSERT_EQUAL(DOF4_INIT_ANGLE, getExpectedDofAngle(dofNumber4));
	TEST_ASSERT_EQUAL(DOF5_INIT_ANGLE, getExpectedDofAngle(dofNumber5));
	TEST_ASSERT_EQUAL(DOF6_INIT_ANGLE, getExpectedDofAngle(dofNumber6));

}

// Expected angle changing tests

void test_angleNotChangingIfRobotIsOff(){

	setDofAngle(54, dofNumber1);
	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE, getExpectedDofAngle(dofNumber1));

}

void test_setDofAngleSetsExpectedAngleProperlyIfRobotOn(){

	uint16_t angle = 54;

	robotStart();

	setDofAngle(angle, dofNumber1);
	TEST_ASSERT_EQUAL(angle, getExpectedDofAngle(dofNumber1));

}

void test_expectedAngleNotSetIfAngleTooHigh(){

	uint16_t angle = DOF1_MAX_ANGLE + 5;

	robotStart();

	setDofAngle(angle, dofNumber1);
	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE, getExpectedDofAngle(dofNumber1));
}

void test_expectedAngleNotSetIfAngleTooLow(){

	uint16_t angle = -2;

	robotStart();

	setDofAngle(angle, dofNumber1);
	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE, getExpectedDofAngle(dofNumber1));
}

// angle change processing

void test_angleIncreasesBy2AfterProcessingAngleChange(){

	uint16_t angle = 92;

	robotStart();

	setDofAngle(angle, dofNumber1);
	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));

}

void test_angleIncreasesBy3AfterProcessingAngleChange(){

	uint16_t angle = 93;

	robotStart();

	setDofAngle(angle, dofNumber1);
	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle - 1, getDofAngle(dofNumber1));

	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));

}

void test_angleDecreasesBy2AfterProcessingAngleChange(){

	uint16_t angle = 88;

	robotStart();

	setDofAngle(angle, dofNumber1);
	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));

}

void test_angleDecreasesBy3AfterProcessingAngleChange(){

	uint16_t angle = 87;

	robotStart();

	setDofAngle(angle, dofNumber1);
	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle + 1, getDofAngle(dofNumber1));

	processAngleChanging(dofNumber1);

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));

}

void test_processingMoreTimeThanItsNeededIsNotHarmful(){

	uint16_t angle = 87;

	robotStart();

	setDofAngle(angle, dofNumber1);

	for(int i = 0; i < 60; i++){
		processAngleChanging(dofNumber1);
	}

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));
}

void test_processingToHighValue(){

	uint16_t angle = 164;

	robotStart();

	setDofAngle(angle, dofNumber1);

	for(int i = 0; i < (angle - DOF1_INIT_ANGLE)/2; i++){
		processAngleChanging(dofNumber1);
	}

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));
}

void test_processingToLowValue(){

	uint16_t angle = 3;

	robotStart();

	setDofAngle(angle, dofNumber1);

	for(int i = 0; i < (DOF1_INIT_ANGLE - angle)/2 + 1; i++){
		processAngleChanging(dofNumber1);
	}

	TEST_ASSERT_EQUAL(angle, getDofAngle(dofNumber1));
}

// Angle increase and decrease functions tests

void test_angleIncreasesAfterAngleIncrease(){

	uint16_t change = 13;

	robotStart();

	increaseDofAngle(change, dofNumber1);

	for(int i = 0; i < (change)/2 + 1; i++){
		processAngleChanging(dofNumber1);
	}

	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE + change, getDofAngle(dofNumber1));

}

void test_angleDecreasesAfterAngleDecrease(){

	uint16_t change = 77;

	robotStart();

	decreaseDofAngle(change, dofNumber1);

	for(int i = 0; i < (change)/2 + 1; i++){
		processAngleChanging(dofNumber1);
	}

	TEST_ASSERT_EQUAL(DOF1_INIT_ANGLE - change, getDofAngle(dofNumber1));

}

// Gripper tests

void test_gripperOpenAfterInit(){

	TEST_ASSERT_EQUAL(GRIPPER_OPEN, getGripperState());
}

void test_gripperCanBeClosed(){

	closeGripper();
	processChangedGripperState();
	TEST_ASSERT_EQUAL(GRIPPER_CLOSE, getGripperState());
}

void test_gripperCanBeOpened(){

	closeGripper();
	processChangedGripperState();
	openGripper();
	processChangedGripperState();
	TEST_ASSERT_EQUAL(GRIPPER_OPEN, getGripperState());
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/





