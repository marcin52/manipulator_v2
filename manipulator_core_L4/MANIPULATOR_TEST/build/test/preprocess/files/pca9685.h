#include "D:/Desktop/Programowanie/Manipulator/manipulator_v2/manipulator_core_L4/CMSIS/device/stm32l4xx.h"
#include "D:/Desktop/Programowanie/Manipulator/manipulator_v2/manipulator_core_L4/inc/main.h"


typedef enum {

 KG25_180_SERVO = 1,

 KG25_270_SERVO = 2,

 POWER_HD_1711MG_SERVO = 3,

 SG90_SERVO = 4,

 FEETECH_FT3325M_SERVO = 5

}servoTypes;

typedef enum{

 PCA9685_MODE1_SUB1_BIT = 3,

 PCA9685_MODE1_SUB2_BIT = 2,

 PCA9685_MODE1_SUB3_BIT = 1

}SubaddressBit;















typedef enum{

 PCA9685_OK = 0,

 PCA9685_ERROR = 1

}PCA9685_STATUS;

PCA9685_STATUS PCA9685_Init();



PCA9685_STATUS PCA9685_SetPwmFrequency(uint16_t Frequency);



PCA9685_STATUS PCA9685_SetChannelPWM(uint8_t Channel,

  uint16_t PWM_Value, uint8_t Invert);





PCA9685_STATUS PCA9685_SetServoAngle(uint8_t Channel,

  float Angle, servoTypes servo);
