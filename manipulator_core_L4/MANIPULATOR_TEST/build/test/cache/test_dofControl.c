#include "build/temp/_test_dofControl.c"
#include "build/test/mocks/mock_pca9685.h"
#include "D:/Desktop/Programowanie/Manipulator/manipulator_v2/manipulator_core_L4/inc/dofControl.h"
#include "C:/Ruby27-x64/lib/ruby/gems/2.7.0/gems/ceedling-0.30.0/vendor/unity/src/unity.h"








void setUp(void)

{

 PCA9685_Init_CMockIgnoreAndReturn(64, 0);

 PCA9685_SetServoAngle_CMockIgnoreAndReturn(65, 0);

 initDofControl();

}



void tearDown(void)

{



}















void test_initAngleDefinitionsMatchWithSource(){



 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(82), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((135)), (UNITY_INT)((getDofAngle(dofNumber2))), (

((void *)0)

), (UNITY_UINT)(83), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((135)), (UNITY_INT)((getDofAngle(dofNumber3))), (

((void *)0)

), (UNITY_UINT)(84), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((60)), (UNITY_INT)((getDofAngle(dofNumber4))), (

((void *)0)

), (UNITY_UINT)(85), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getDofAngle(dofNumber5))), (

((void *)0)

), (UNITY_UINT)(86), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getDofAngle(dofNumber6))), (

((void *)0)

), (UNITY_UINT)(87), UNITY_DISPLAY_STYLE_INT);



}



void test_maxAngleDefinitionsMatchWithSource(){



 UnityAssertEqualNumber((UNITY_INT)((180)), (UNITY_INT)((getDofMaxAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(93), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((270)), (UNITY_INT)((getDofMaxAngle(dofNumber2))), (

((void *)0)

), (UNITY_UINT)(94), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((270)), (UNITY_INT)((getDofMaxAngle(dofNumber3))), (

((void *)0)

), (UNITY_UINT)(95), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((180)), (UNITY_INT)((getDofMaxAngle(dofNumber4))), (

((void *)0)

), (UNITY_UINT)(96), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((180)), (UNITY_INT)((getDofMaxAngle(dofNumber5))), (

((void *)0)

), (UNITY_UINT)(97), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((180)), (UNITY_INT)((getDofMaxAngle(dofNumber6))), (

((void *)0)

), (UNITY_UINT)(98), UNITY_DISPLAY_STYLE_INT);



}



void test_initExpectedAnglesAreInitAngles(){



 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(104), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((135)), (UNITY_INT)((getExpectedDofAngle(dofNumber2))), (

((void *)0)

), (UNITY_UINT)(105), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((135)), (UNITY_INT)((getExpectedDofAngle(dofNumber3))), (

((void *)0)

), (UNITY_UINT)(106), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((60)), (UNITY_INT)((getExpectedDofAngle(dofNumber4))), (

((void *)0)

), (UNITY_UINT)(107), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber5))), (

((void *)0)

), (UNITY_UINT)(108), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber6))), (

((void *)0)

), (UNITY_UINT)(109), UNITY_DISPLAY_STYLE_INT);



}







void test_angleNotChangingIfRobotIsOff(){



 setDofAngle(54, dofNumber1);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(118), UNITY_DISPLAY_STYLE_INT);



}



void test_setDofAngleSetsExpectedAngleProperlyIfRobotOn(){



 uint16_t angle = 54;



 robotStart();



 setDofAngle(angle, dofNumber1);

 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getExpectedDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(129), UNITY_DISPLAY_STYLE_INT);



}



void test_expectedAngleNotSetIfAngleTooHigh(){



 uint16_t angle = 180 + 5;



 robotStart();



 setDofAngle(angle, dofNumber1);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(140), UNITY_DISPLAY_STYLE_INT);

}



void test_expectedAngleNotSetIfAngleTooLow(){



 uint16_t angle = -2;



 robotStart();



 setDofAngle(angle, dofNumber1);

 UnityAssertEqualNumber((UNITY_INT)((90)), (UNITY_INT)((getExpectedDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(150), UNITY_DISPLAY_STYLE_INT);

}







void test_angleIncreasesBy2AfterProcessingAngleChange(){



 uint16_t angle = 92;



 robotStart();



 setDofAngle(angle, dofNumber1);

 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(164), UNITY_DISPLAY_STYLE_INT);



}



void test_angleIncreasesBy3AfterProcessingAngleChange(){



 uint16_t angle = 93;



 robotStart();



 setDofAngle(angle, dofNumber1);

 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle - 1)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(177), UNITY_DISPLAY_STYLE_INT);



 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(181), UNITY_DISPLAY_STYLE_INT);



}



void test_angleDecreasesBy2AfterProcessingAngleChange(){



 uint16_t angle = 88;



 robotStart();



 setDofAngle(angle, dofNumber1);

 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(194), UNITY_DISPLAY_STYLE_INT);



}



void test_angleDecreasesBy3AfterProcessingAngleChange(){



 uint16_t angle = 87;



 robotStart();



 setDofAngle(angle, dofNumber1);

 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle + 1)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(207), UNITY_DISPLAY_STYLE_INT);



 processAngleChanging(dofNumber1);



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(211), UNITY_DISPLAY_STYLE_INT);



}



void test_processingMoreTimeThanItsNeededIsNotHarmful(){



 uint16_t angle = 87;



 robotStart();



 setDofAngle(angle, dofNumber1);



 for(int i = 0; i < 60; i++){

  processAngleChanging(dofNumber1);

 }



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(227), UNITY_DISPLAY_STYLE_INT);

}



void test_processingToHighValue(){



 uint16_t angle = 164;



 robotStart();



 setDofAngle(angle, dofNumber1);



 for(int i = 0; i < (angle - 90)/2; i++){

  processAngleChanging(dofNumber1);

 }



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(242), UNITY_DISPLAY_STYLE_INT);

}



void test_processingToLowValue(){



 uint16_t angle = 3;



 robotStart();



 setDofAngle(angle, dofNumber1);



 for(int i = 0; i < (90 - angle)/2 + 1; i++){

  processAngleChanging(dofNumber1);

 }



 UnityAssertEqualNumber((UNITY_INT)((angle)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(257), UNITY_DISPLAY_STYLE_INT);

}







void test_angleIncreasesAfterAngleIncrease(){



 uint16_t change = 13;



 robotStart();



 increaseDofAngle(change, dofNumber1);



 for(int i = 0; i < (change)/2 + 1; i++){

  processAngleChanging(dofNumber1);

 }



 UnityAssertEqualNumber((UNITY_INT)((90 + change)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(274), UNITY_DISPLAY_STYLE_INT);



}



void test_angleDecreasesAfterAngleDecrease(){



 uint16_t change = 77;



 robotStart();



 decreaseDofAngle(change, dofNumber1);



 for(int i = 0; i < (change)/2 + 1; i++){

  processAngleChanging(dofNumber1);

 }



 UnityAssertEqualNumber((UNITY_INT)((90 - change)), (UNITY_INT)((getDofAngle(dofNumber1))), (

((void *)0)

), (UNITY_UINT)(290), UNITY_DISPLAY_STYLE_INT);



}





void test_gripperOpenAfterInit(){



 UnityAssertEqualNumber((UNITY_INT)((GRIPPER_OPEN)), (UNITY_INT)((getGripperState())), (

((void *)0)

), (UNITY_UINT)(297), UNITY_DISPLAY_STYLE_INT);

}



void test_gripperCanBeClosed(){



 closeGripper();

 processChangedGripperState();

 UnityAssertEqualNumber((UNITY_INT)((GRIPPER_CLOSE)), (UNITY_INT)((getGripperState())), (

((void *)0)

), (UNITY_UINT)(304), UNITY_DISPLAY_STYLE_INT);

}



void test_gripperCanBeOpened(){



 closeGripper();

 processChangedGripperState();

 openGripper();

 processChangedGripperState();

 UnityAssertEqualNumber((UNITY_INT)((GRIPPER_OPEN)), (UNITY_INT)((getGripperState())), (

((void *)0)

), (UNITY_UINT)(313), UNITY_DISPLAY_STYLE_INT);

}
