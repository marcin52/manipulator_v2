/* AUTOGENERATED FILE. DO NOT EDIT. */
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include "cmock.h"
#include "mock_pca9685.h"

static const char* CMockString_Angle = "Angle";
static const char* CMockString_Channel = "Channel";
static const char* CMockString_Frequency = "Frequency";
static const char* CMockString_Invert = "Invert";
static const char* CMockString_PCA9685_Init = "PCA9685_Init";
static const char* CMockString_PCA9685_SetChannelPWM = "PCA9685_SetChannelPWM";
static const char* CMockString_PCA9685_SetPwmFrequency = "PCA9685_SetPwmFrequency";
static const char* CMockString_PCA9685_SetServoAngle = "PCA9685_SetServoAngle";
static const char* CMockString_PWM_Value = "PWM_Value";
static const char* CMockString_servo = "servo";

typedef struct _CMOCK_PCA9685_Init_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  PCA9685_STATUS ReturnVal;
  int CallOrder;

} CMOCK_PCA9685_Init_CALL_INSTANCE;

typedef struct _CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  PCA9685_STATUS ReturnVal;
  int CallOrder;
  uint16_t Expected_Frequency;
  char IgnoreArg_Frequency;

} CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE;

typedef struct _CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  PCA9685_STATUS ReturnVal;
  int CallOrder;
  uint8_t Expected_Channel;
  uint16_t Expected_PWM_Value;
  uint8_t Expected_Invert;
  char IgnoreArg_Channel;
  char IgnoreArg_PWM_Value;
  char IgnoreArg_Invert;

} CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE;

typedef struct _CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  PCA9685_STATUS ReturnVal;
  int CallOrder;
  uint8_t Expected_Channel;
  float Expected_Angle;
  servoTypes Expected_servo;
  char IgnoreArg_Channel;
  char IgnoreArg_Angle;
  char IgnoreArg_servo;

} CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE;

static struct mock_pca9685Instance
{
  char PCA9685_Init_IgnoreBool;
  PCA9685_STATUS PCA9685_Init_FinalReturn;
  char PCA9685_Init_CallbackBool;
  CMOCK_PCA9685_Init_CALLBACK PCA9685_Init_CallbackFunctionPointer;
  int PCA9685_Init_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE PCA9685_Init_CallInstance;
  char PCA9685_SetPwmFrequency_IgnoreBool;
  PCA9685_STATUS PCA9685_SetPwmFrequency_FinalReturn;
  char PCA9685_SetPwmFrequency_CallbackBool;
  CMOCK_PCA9685_SetPwmFrequency_CALLBACK PCA9685_SetPwmFrequency_CallbackFunctionPointer;
  int PCA9685_SetPwmFrequency_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE PCA9685_SetPwmFrequency_CallInstance;
  char PCA9685_SetChannelPWM_IgnoreBool;
  PCA9685_STATUS PCA9685_SetChannelPWM_FinalReturn;
  char PCA9685_SetChannelPWM_CallbackBool;
  CMOCK_PCA9685_SetChannelPWM_CALLBACK PCA9685_SetChannelPWM_CallbackFunctionPointer;
  int PCA9685_SetChannelPWM_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE PCA9685_SetChannelPWM_CallInstance;
  char PCA9685_SetServoAngle_IgnoreBool;
  PCA9685_STATUS PCA9685_SetServoAngle_FinalReturn;
  char PCA9685_SetServoAngle_CallbackBool;
  CMOCK_PCA9685_SetServoAngle_CALLBACK PCA9685_SetServoAngle_CallbackFunctionPointer;
  int PCA9685_SetServoAngle_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE PCA9685_SetServoAngle_CallInstance;
} Mock;

extern jmp_buf AbortFrame;
extern int GlobalExpectCount;
extern int GlobalVerifyOrder;

void mock_pca9685_Verify(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_MEM_INDEX_TYPE call_instance;
  call_instance = Mock.PCA9685_Init_CallInstance;
  if (Mock.PCA9685_Init_IgnoreBool)
    call_instance = CMOCK_GUTS_NONE;
  if (CMOCK_GUTS_NONE != call_instance)
  {
    UNITY_SET_DETAIL(CMockString_PCA9685_Init);
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLess);
  }
  if (Mock.PCA9685_Init_CallbackFunctionPointer != NULL)
  {
    call_instance = CMOCK_GUTS_NONE;
    (void)call_instance;
  }
  call_instance = Mock.PCA9685_SetPwmFrequency_CallInstance;
  if (Mock.PCA9685_SetPwmFrequency_IgnoreBool)
    call_instance = CMOCK_GUTS_NONE;
  if (CMOCK_GUTS_NONE != call_instance)
  {
    UNITY_SET_DETAIL(CMockString_PCA9685_SetPwmFrequency);
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLess);
  }
  if (Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer != NULL)
  {
    call_instance = CMOCK_GUTS_NONE;
    (void)call_instance;
  }
  call_instance = Mock.PCA9685_SetChannelPWM_CallInstance;
  if (Mock.PCA9685_SetChannelPWM_IgnoreBool)
    call_instance = CMOCK_GUTS_NONE;
  if (CMOCK_GUTS_NONE != call_instance)
  {
    UNITY_SET_DETAIL(CMockString_PCA9685_SetChannelPWM);
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLess);
  }
  if (Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer != NULL)
  {
    call_instance = CMOCK_GUTS_NONE;
    (void)call_instance;
  }
  call_instance = Mock.PCA9685_SetServoAngle_CallInstance;
  if (Mock.PCA9685_SetServoAngle_IgnoreBool)
    call_instance = CMOCK_GUTS_NONE;
  if (CMOCK_GUTS_NONE != call_instance)
  {
    UNITY_SET_DETAIL(CMockString_PCA9685_SetServoAngle);
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLess);
  }
  if (Mock.PCA9685_SetServoAngle_CallbackFunctionPointer != NULL)
  {
    call_instance = CMOCK_GUTS_NONE;
    (void)call_instance;
  }
}

void mock_pca9685_Init(void)
{
  mock_pca9685_Destroy();
}

void mock_pca9685_Destroy(void)
{
  CMock_Guts_MemFreeAll();
  memset(&Mock, 0, sizeof(Mock));
  GlobalExpectCount = 0;
  GlobalVerifyOrder = 0;
}

PCA9685_STATUS PCA9685_Init(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_PCA9685_Init_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_PCA9685_Init);
  cmock_call_instance = (CMOCK_PCA9685_Init_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.PCA9685_Init_CallInstance);
  Mock.PCA9685_Init_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_Init_CallInstance);
  if (Mock.PCA9685_Init_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.PCA9685_Init_FinalReturn;
    memcpy((void*)(&Mock.PCA9685_Init_FinalReturn), (void*)(&cmock_call_instance->ReturnVal),
         sizeof(PCA9685_STATUS[sizeof(cmock_call_instance->ReturnVal) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
    return cmock_call_instance->ReturnVal;
  }
  if (!Mock.PCA9685_Init_CallbackBool &&
      Mock.PCA9685_Init_CallbackFunctionPointer != NULL)
  {
    PCA9685_STATUS cmock_cb_ret = Mock.PCA9685_Init_CallbackFunctionPointer(Mock.PCA9685_Init_CallbackCalls++);
    UNITY_CLR_DETAILS();
    return cmock_cb_ret;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->CallOrder > ++GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledEarly);
  if (cmock_call_instance->CallOrder < GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLate);
  if (Mock.PCA9685_Init_CallbackFunctionPointer != NULL)
  {
    cmock_call_instance->ReturnVal = Mock.PCA9685_Init_CallbackFunctionPointer(Mock.PCA9685_Init_CallbackCalls++);
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void PCA9685_Init_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_Init_CALL_INSTANCE));
  CMOCK_PCA9685_Init_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_Init_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_Init_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_Init_CallInstance, cmock_guts_index);
  Mock.PCA9685_Init_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.PCA9685_Init_IgnoreBool = (char)1;
}

void PCA9685_Init_CMockStopIgnore(void)
{
  if(Mock.PCA9685_Init_IgnoreBool)
    Mock.PCA9685_Init_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_Init_CallInstance);
  Mock.PCA9685_Init_IgnoreBool = (char)0;
}

void PCA9685_Init_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_Init_CALL_INSTANCE));
  CMOCK_PCA9685_Init_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_Init_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_Init_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_Init_CallInstance, cmock_guts_index);
  Mock.PCA9685_Init_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->CallOrder = ++GlobalExpectCount;
  memcpy((void*)(&cmock_call_instance->ReturnVal), (void*)(&cmock_to_return),
         sizeof(PCA9685_STATUS[sizeof(cmock_to_return) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
}

void PCA9685_Init_AddCallback(CMOCK_PCA9685_Init_CALLBACK Callback)
{
  Mock.PCA9685_Init_IgnoreBool = (char)0;
  Mock.PCA9685_Init_CallbackBool = (char)1;
  Mock.PCA9685_Init_CallbackFunctionPointer = Callback;
}

void PCA9685_Init_Stub(CMOCK_PCA9685_Init_CALLBACK Callback)
{
  Mock.PCA9685_Init_IgnoreBool = (char)0;
  Mock.PCA9685_Init_CallbackBool = (char)0;
  Mock.PCA9685_Init_CallbackFunctionPointer = Callback;
}

PCA9685_STATUS PCA9685_SetPwmFrequency(uint16_t Frequency)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_PCA9685_SetPwmFrequency);
  cmock_call_instance = (CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.PCA9685_SetPwmFrequency_CallInstance);
  Mock.PCA9685_SetPwmFrequency_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetPwmFrequency_CallInstance);
  if (Mock.PCA9685_SetPwmFrequency_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.PCA9685_SetPwmFrequency_FinalReturn;
    memcpy((void*)(&Mock.PCA9685_SetPwmFrequency_FinalReturn), (void*)(&cmock_call_instance->ReturnVal),
         sizeof(PCA9685_STATUS[sizeof(cmock_call_instance->ReturnVal) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
    return cmock_call_instance->ReturnVal;
  }
  if (!Mock.PCA9685_SetPwmFrequency_CallbackBool &&
      Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer != NULL)
  {
    PCA9685_STATUS cmock_cb_ret = Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer(Frequency, Mock.PCA9685_SetPwmFrequency_CallbackCalls++);
    UNITY_CLR_DETAILS();
    return cmock_cb_ret;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->CallOrder > ++GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledEarly);
  if (cmock_call_instance->CallOrder < GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLate);
  if (!cmock_call_instance->IgnoreArg_Frequency)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetPwmFrequency,CMockString_Frequency);
    UNITY_TEST_ASSERT_EQUAL_HEX16(cmock_call_instance->Expected_Frequency, Frequency, cmock_line, CMockStringMismatch);
  }
  if (Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer != NULL)
  {
    cmock_call_instance->ReturnVal = Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer(Frequency, Mock.PCA9685_SetPwmFrequency_CallbackCalls++);
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void CMockExpectParameters_PCA9685_SetPwmFrequency(CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE* cmock_call_instance, uint16_t Frequency)
{
  cmock_call_instance->Expected_Frequency = Frequency;
  cmock_call_instance->IgnoreArg_Frequency = 0;
}

void PCA9685_SetPwmFrequency_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE));
  CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetPwmFrequency_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetPwmFrequency_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)1;
}

void PCA9685_SetPwmFrequency_CMockStopIgnore(void)
{
  if(Mock.PCA9685_SetPwmFrequency_IgnoreBool)
    Mock.PCA9685_SetPwmFrequency_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetPwmFrequency_CallInstance);
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)0;
}

void PCA9685_SetPwmFrequency_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint16_t Frequency, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE));
  CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetPwmFrequency_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetPwmFrequency_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->CallOrder = ++GlobalExpectCount;
  CMockExpectParameters_PCA9685_SetPwmFrequency(cmock_call_instance, Frequency);
  memcpy((void*)(&cmock_call_instance->ReturnVal), (void*)(&cmock_to_return),
         sizeof(PCA9685_STATUS[sizeof(cmock_to_return) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
}

void PCA9685_SetPwmFrequency_AddCallback(CMOCK_PCA9685_SetPwmFrequency_CALLBACK Callback)
{
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)0;
  Mock.PCA9685_SetPwmFrequency_CallbackBool = (char)1;
  Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer = Callback;
}

void PCA9685_SetPwmFrequency_Stub(CMOCK_PCA9685_SetPwmFrequency_CALLBACK Callback)
{
  Mock.PCA9685_SetPwmFrequency_IgnoreBool = (char)0;
  Mock.PCA9685_SetPwmFrequency_CallbackBool = (char)0;
  Mock.PCA9685_SetPwmFrequency_CallbackFunctionPointer = Callback;
}

void PCA9685_SetPwmFrequency_CMockIgnoreArg_Frequency(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetPwmFrequency_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetPwmFrequency_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_Frequency = 1;
}

PCA9685_STATUS PCA9685_SetChannelPWM(uint8_t Channel, uint16_t PWM_Value, uint8_t Invert)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_PCA9685_SetChannelPWM);
  cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.PCA9685_SetChannelPWM_CallInstance);
  Mock.PCA9685_SetChannelPWM_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetChannelPWM_CallInstance);
  if (Mock.PCA9685_SetChannelPWM_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.PCA9685_SetChannelPWM_FinalReturn;
    memcpy((void*)(&Mock.PCA9685_SetChannelPWM_FinalReturn), (void*)(&cmock_call_instance->ReturnVal),
         sizeof(PCA9685_STATUS[sizeof(cmock_call_instance->ReturnVal) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
    return cmock_call_instance->ReturnVal;
  }
  if (!Mock.PCA9685_SetChannelPWM_CallbackBool &&
      Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer != NULL)
  {
    PCA9685_STATUS cmock_cb_ret = Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer(Channel, PWM_Value, Invert, Mock.PCA9685_SetChannelPWM_CallbackCalls++);
    UNITY_CLR_DETAILS();
    return cmock_cb_ret;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->CallOrder > ++GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledEarly);
  if (cmock_call_instance->CallOrder < GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLate);
  if (!cmock_call_instance->IgnoreArg_Channel)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetChannelPWM,CMockString_Channel);
    UNITY_TEST_ASSERT_EQUAL_HEX8(cmock_call_instance->Expected_Channel, Channel, cmock_line, CMockStringMismatch);
  }
  if (!cmock_call_instance->IgnoreArg_PWM_Value)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetChannelPWM,CMockString_PWM_Value);
    UNITY_TEST_ASSERT_EQUAL_HEX16(cmock_call_instance->Expected_PWM_Value, PWM_Value, cmock_line, CMockStringMismatch);
  }
  if (!cmock_call_instance->IgnoreArg_Invert)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetChannelPWM,CMockString_Invert);
    UNITY_TEST_ASSERT_EQUAL_HEX8(cmock_call_instance->Expected_Invert, Invert, cmock_line, CMockStringMismatch);
  }
  if (Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer != NULL)
  {
    cmock_call_instance->ReturnVal = Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer(Channel, PWM_Value, Invert, Mock.PCA9685_SetChannelPWM_CallbackCalls++);
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void CMockExpectParameters_PCA9685_SetChannelPWM(CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance, uint8_t Channel, uint16_t PWM_Value, uint8_t Invert)
{
  cmock_call_instance->Expected_Channel = Channel;
  cmock_call_instance->IgnoreArg_Channel = 0;
  cmock_call_instance->Expected_PWM_Value = PWM_Value;
  cmock_call_instance->IgnoreArg_PWM_Value = 0;
  cmock_call_instance->Expected_Invert = Invert;
  cmock_call_instance->IgnoreArg_Invert = 0;
}

void PCA9685_SetChannelPWM_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE));
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetChannelPWM_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetChannelPWM_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)1;
}

void PCA9685_SetChannelPWM_CMockStopIgnore(void)
{
  if(Mock.PCA9685_SetChannelPWM_IgnoreBool)
    Mock.PCA9685_SetChannelPWM_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetChannelPWM_CallInstance);
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)0;
}

void PCA9685_SetChannelPWM_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint8_t Channel, uint16_t PWM_Value, uint8_t Invert, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE));
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetChannelPWM_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetChannelPWM_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->CallOrder = ++GlobalExpectCount;
  CMockExpectParameters_PCA9685_SetChannelPWM(cmock_call_instance, Channel, PWM_Value, Invert);
  memcpy((void*)(&cmock_call_instance->ReturnVal), (void*)(&cmock_to_return),
         sizeof(PCA9685_STATUS[sizeof(cmock_to_return) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
}

void PCA9685_SetChannelPWM_AddCallback(CMOCK_PCA9685_SetChannelPWM_CALLBACK Callback)
{
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)0;
  Mock.PCA9685_SetChannelPWM_CallbackBool = (char)1;
  Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer = Callback;
}

void PCA9685_SetChannelPWM_Stub(CMOCK_PCA9685_SetChannelPWM_CALLBACK Callback)
{
  Mock.PCA9685_SetChannelPWM_IgnoreBool = (char)0;
  Mock.PCA9685_SetChannelPWM_CallbackBool = (char)0;
  Mock.PCA9685_SetChannelPWM_CallbackFunctionPointer = Callback;
}

void PCA9685_SetChannelPWM_CMockIgnoreArg_Channel(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetChannelPWM_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_Channel = 1;
}

void PCA9685_SetChannelPWM_CMockIgnoreArg_PWM_Value(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetChannelPWM_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_PWM_Value = 1;
}

void PCA9685_SetChannelPWM_CMockIgnoreArg_Invert(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetChannelPWM_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetChannelPWM_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_Invert = 1;
}

PCA9685_STATUS PCA9685_SetServoAngle(uint8_t Channel, float Angle, servoTypes servo)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_PCA9685_SetServoAngle);
  cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.PCA9685_SetServoAngle_CallInstance);
  Mock.PCA9685_SetServoAngle_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetServoAngle_CallInstance);
  if (Mock.PCA9685_SetServoAngle_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.PCA9685_SetServoAngle_FinalReturn;
    memcpy((void*)(&Mock.PCA9685_SetServoAngle_FinalReturn), (void*)(&cmock_call_instance->ReturnVal),
         sizeof(PCA9685_STATUS[sizeof(cmock_call_instance->ReturnVal) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
    return cmock_call_instance->ReturnVal;
  }
  if (!Mock.PCA9685_SetServoAngle_CallbackBool &&
      Mock.PCA9685_SetServoAngle_CallbackFunctionPointer != NULL)
  {
    PCA9685_STATUS cmock_cb_ret = Mock.PCA9685_SetServoAngle_CallbackFunctionPointer(Channel, Angle, servo, Mock.PCA9685_SetServoAngle_CallbackCalls++);
    UNITY_CLR_DETAILS();
    return cmock_cb_ret;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->CallOrder > ++GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledEarly);
  if (cmock_call_instance->CallOrder < GlobalVerifyOrder)
    UNITY_TEST_FAIL(cmock_line, CMockStringCalledLate);
  if (!cmock_call_instance->IgnoreArg_Channel)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetServoAngle,CMockString_Channel);
    UNITY_TEST_ASSERT_EQUAL_HEX8(cmock_call_instance->Expected_Channel, Channel, cmock_line, CMockStringMismatch);
  }
  if (!cmock_call_instance->IgnoreArg_Angle)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetServoAngle,CMockString_Angle);
    UNITY_TEST_ASSERT_EQUAL_FLOAT(cmock_call_instance->Expected_Angle, Angle, cmock_line, CMockStringMismatch);
  }
  if (!cmock_call_instance->IgnoreArg_servo)
  {
    UNITY_SET_DETAILS(CMockString_PCA9685_SetServoAngle,CMockString_servo);
    UNITY_TEST_ASSERT_EQUAL_MEMORY((void*)(&cmock_call_instance->Expected_servo), (void*)(&servo), sizeof(servoTypes), cmock_line, CMockStringMismatch);
  }
  if (Mock.PCA9685_SetServoAngle_CallbackFunctionPointer != NULL)
  {
    cmock_call_instance->ReturnVal = Mock.PCA9685_SetServoAngle_CallbackFunctionPointer(Channel, Angle, servo, Mock.PCA9685_SetServoAngle_CallbackCalls++);
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void CMockExpectParameters_PCA9685_SetServoAngle(CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance, uint8_t Channel, float Angle, servoTypes servo)
{
  cmock_call_instance->Expected_Channel = Channel;
  cmock_call_instance->IgnoreArg_Channel = 0;
  cmock_call_instance->Expected_Angle = Angle;
  cmock_call_instance->IgnoreArg_Angle = 0;
  memcpy((void*)(&cmock_call_instance->Expected_servo), (void*)(&servo),
         sizeof(servoTypes[sizeof(servo) == sizeof(servoTypes) ? 1 : -1])); /* add servoTypes to :treat_as_array if this causes an error */
  cmock_call_instance->IgnoreArg_servo = 0;
}

void PCA9685_SetServoAngle_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE));
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetServoAngle_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetServoAngle_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)1;
}

void PCA9685_SetServoAngle_CMockStopIgnore(void)
{
  if(Mock.PCA9685_SetServoAngle_IgnoreBool)
    Mock.PCA9685_SetServoAngle_CallInstance = CMock_Guts_MemNext(Mock.PCA9685_SetServoAngle_CallInstance);
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)0;
}

void PCA9685_SetServoAngle_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint8_t Channel, float Angle, servoTypes servo, PCA9685_STATUS cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE));
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.PCA9685_SetServoAngle_CallInstance = CMock_Guts_MemChain(Mock.PCA9685_SetServoAngle_CallInstance, cmock_guts_index);
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->CallOrder = ++GlobalExpectCount;
  CMockExpectParameters_PCA9685_SetServoAngle(cmock_call_instance, Channel, Angle, servo);
  memcpy((void*)(&cmock_call_instance->ReturnVal), (void*)(&cmock_to_return),
         sizeof(PCA9685_STATUS[sizeof(cmock_to_return) == sizeof(PCA9685_STATUS) ? 1 : -1])); /* add PCA9685_STATUS to :treat_as_array if this causes an error */
}

void PCA9685_SetServoAngle_AddCallback(CMOCK_PCA9685_SetServoAngle_CALLBACK Callback)
{
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)0;
  Mock.PCA9685_SetServoAngle_CallbackBool = (char)1;
  Mock.PCA9685_SetServoAngle_CallbackFunctionPointer = Callback;
}

void PCA9685_SetServoAngle_Stub(CMOCK_PCA9685_SetServoAngle_CALLBACK Callback)
{
  Mock.PCA9685_SetServoAngle_IgnoreBool = (char)0;
  Mock.PCA9685_SetServoAngle_CallbackBool = (char)0;
  Mock.PCA9685_SetServoAngle_CallbackFunctionPointer = Callback;
}

void PCA9685_SetServoAngle_CMockIgnoreArg_Channel(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetServoAngle_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_Channel = 1;
}

void PCA9685_SetServoAngle_CMockIgnoreArg_Angle(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetServoAngle_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_Angle = 1;
}

void PCA9685_SetServoAngle_CMockIgnoreArg_servo(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE* cmock_call_instance = (CMOCK_PCA9685_SetServoAngle_CALL_INSTANCE*)CMock_Guts_GetAddressFor(CMock_Guts_MemEndOfChain(Mock.PCA9685_SetServoAngle_CallInstance));
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringIgnPreExp);
  cmock_call_instance->IgnoreArg_servo = 1;
}

