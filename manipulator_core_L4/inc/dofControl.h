/*
 * dofControl.h
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

#ifndef SERVOCONTROL_DOFCONTROL_H_
#define SERVOCONTROL_DOFCONTROL_H_

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/
//	Enable Servo control mode for pca9685.h file proper compilation
#define PCA9685_SERVO_MODE

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

typedef enum{
	GRIPPER_CLOSE = 0,
	GRIPPER_OPEN = 1
}gripperState_t;

typedef enum{
	ROBOT_ON,
	ROBOT_OFF
}robotState_t;

typedef enum{
	dofNumber1 = 0,
	dofNumber2,
	dofNumber3,
	dofNumber4,
	dofNumber5,
	dofNumber6,
	gripperDofNumber
}dofNumbers_t;
/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/
void initDofControl();
void robotStart();
void robotStop();
robotState_t getRobotState();

// DOF control
void increaseDofAngle(uint16_t value, dofNumbers_t dofNumber);
void decreaseDofAngle(uint16_t value, dofNumbers_t dofNumber);
void setDofAngle(uint16_t angle, dofNumbers_t dofNumber);
uint16_t getDofAngle(dofNumbers_t dofNumber);
uint16_t getDofMaxAngle(dofNumbers_t dofNumber);
uint16_t getExpectedDofAngle(dofNumbers_t dofNumber);

void processAngleChanging(dofNumbers_t dofNumber);

// Gripper control
void closeGripper();
void openGripper();
void processChangedGripperState();
gripperState_t getGripperState();
/*******************************************************************************
 *    end of file
 ******************************************************************************/

#endif /* SERVOCONTROL_DOFCONTROL_H_ */
