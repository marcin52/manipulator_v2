/*
 * uart.h
 *
 *  Created on: 01.03.2020
 *      Author: marci
 */

#ifndef UART_H_
#define UART_H_

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

// interface that defines way to transfer message to the main
#include "communicationService.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/
// Messages configuration
#define COM_MESSAGE_START_SIZE			1
#define COM_MESSAGE_STOP_SIZE			1
#define MESSAGE_SIZE					(MESSAGE_MAX_MESSAGE_SIZE + COM_MESSAGE_START_SIZE + COM_MESSAGE_STOP_SIZE + MESSAGE_ID_SIZE)
#define RX_BUFFER_SIZE 					(MESSAGE_SIZE*2)
#define TX_BUFFER_SIZE 					(MESSAGE_SIZE)

#define MESSAGE_START_CHAR_POSITION		0
#define MESSAGE_ID_CHAR_POSITION		(MESSAGE_START_CHAR_POSITION + COM_MESSAGE_START_SIZE)
#define MESSAGE_VALUE_CHARS_POSITION	(MESSAGE_ID_CHAR_POSITION + MESSAGE_ID_SIZE)
#define MESSAGE_STOP_CHAR_POSITION		(MESSAGE_VALUE_CHARS_POSITION + MESSAGE_MAX_MESSAGE_SIZE)

#define MESSAGE_START_CHAR				'{'
#define MESSAGE_END_CHAR				'}'
/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

// Module Initialization
void uartComInit(void);

// Receiving messages
void convertDataToMessages();
void udartReceiveNextMessage(uint8_t position);

// Sending messages
void uartSendNextMessage(void);

#endif /* UART_H_ */

/*******************************************************************************
 *    end of file
 ******************************************************************************/

