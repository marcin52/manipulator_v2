/*
 * core.h
 *
 *  Created on: 20.03.2020
 *      Author: marci
 */

#ifndef CORE_H_
#define CORE_H_

void SystemClock_Config(void);
void Error_handler(void);

#endif /* CORE_H_ */
