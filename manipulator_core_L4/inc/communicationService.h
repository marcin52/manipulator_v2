/*
 * communicationService.h
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

#ifndef COMMUNICATION_COMMUNICATIONSERVICE_H_
#define COMMUNICATION_COMMUNICATIONSERVICE_H_

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define MESSAGE_BUFFER_SIZE					60
#define MESSAGE_MAX_MESSAGE_SIZE			4
#define MESSAGE_ID_SIZE						1

// Special messages ID (buffer dependend)

#define BUFFER_FULL_ID						1
#define BUFFER_EMPTY_ID						2

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/
typedef struct message_t{
	uint8_t ID;
	uint8_t message[MESSAGE_MAX_MESSAGE_SIZE];
}message_TypeDef;

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

// Initialization
void communicationServiceInit();

// Receiving messages
uint8_t getNextMessageID(void);
message_TypeDef getNextMessage(void);
void deleteNextMessage(void);
void commPutMessageToTheBuffer(message_TypeDef message);

// Sending messages
void sendMessage(message_TypeDef message);
message_TypeDef commGetNextMessage(void);

// Buffer utilities
bool isInputBufferEmpty(void);
bool isOutputBufferEmpty(void);
bool isInputBufferFull(void);
bool isOutputBufferFull(void);
void clearInputBuffer(void);
void clearOutputBuffer(void);

/*******************************************************************************
 *    end of file
 ******************************************************************************/

#endif /* COMMUNICATION_COMMUNICATIONSERVICE_H_ */
