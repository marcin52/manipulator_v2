/*
 * communicationService.c
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include "communicationService.h"
#include "uart.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

typedef struct message_buffer_t{

	message_TypeDef message[MESSAGE_BUFFER_SIZE];
	uint8_t head;
	uint8_t tail;

}messageBuffer_TypeDef;

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

static messageBuffer_TypeDef inputBuffer;
static messageBuffer_TypeDef outputBuffer;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/
// Initialization
void communicationServiceInit(){
	uartComInit();
}

// Receiving messages
uint8_t getNextMessageID(void);

message_TypeDef getNextMessage(void){

	message_TypeDef messageReceived;

	//convertDataToMessages();

	if( !isInputBufferEmpty()){

		if(inputBuffer.tail < MESSAGE_BUFFER_SIZE)
			messageReceived = inputBuffer.message[inputBuffer.tail];

		if(inputBuffer.tail == MESSAGE_BUFFER_SIZE) inputBuffer.tail = -1;
		inputBuffer.tail += 1;

	} else
		messageReceived.ID = BUFFER_EMPTY_ID;

	return messageReceived;
}

void deleteNextMessage(void);

void commPutMessageToTheBuffer(message_TypeDef message){

	if( !isInputBufferFull()){

		if(inputBuffer.head < MESSAGE_BUFFER_SIZE)
			inputBuffer.message[inputBuffer.head] = message;

		if(inputBuffer.head == MESSAGE_BUFFER_SIZE) inputBuffer.head = -1;
		inputBuffer.head += 1;
	}
}

// Sending messages

void sendMessage(message_TypeDef message){

	if( !isOutputBufferFull()){

		if(outputBuffer.head < MESSAGE_BUFFER_SIZE)
			outputBuffer.message[outputBuffer.head] = message;

		if(outputBuffer.head == MESSAGE_BUFFER_SIZE) outputBuffer.head = -1;
		outputBuffer.head += 1;

	}
}

message_TypeDef commGetNextMessage(void){

	message_TypeDef messageSended;

	if( !isOutputBufferEmpty()){

		if(outputBuffer.tail < MESSAGE_BUFFER_SIZE)
			messageSended = outputBuffer.message[outputBuffer.tail];

		if(outputBuffer.tail == MESSAGE_BUFFER_SIZE) outputBuffer.tail = -1;
		outputBuffer.tail += 1;

	} else
		messageSended.ID = BUFFER_EMPTY_ID;

	return messageSended;
}

// Buffer utilities

bool isInputBufferEmpty(void){

	if(inputBuffer.head == inputBuffer.tail)
		return true;
	else
		return false;
}

bool isOutputBufferEmpty(void){

	if(outputBuffer.head == outputBuffer.tail)
		return true;
	else
		return false;
}

bool isInputBufferFull(void){

	uint8_t tmp_head = inputBuffer.head + 1;
	if( tmp_head > MESSAGE_BUFFER_SIZE) tmp_head = 0;

	if( tmp_head == inputBuffer.tail )
		return true;
	else
		return false;
}

bool isOutputBufferFull(void){

	uint8_t tmp_head = outputBuffer.head + 1;
	if( tmp_head > MESSAGE_BUFFER_SIZE) tmp_head = 0;

	if( tmp_head == outputBuffer.tail )
		return true;
	else
		return false;
}

void clearInputBuffer(void){

	inputBuffer.head = 0;
	inputBuffer.tail = 0;

}
void clearOutputBuffer(void){

	outputBuffer.head = 0;
	outputBuffer.tail = 0;
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/
