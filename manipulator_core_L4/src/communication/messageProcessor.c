/*
 * messageProcessor.c
 *
 *  Created on: 6 lis 2020
 *      Author: marci
 */

/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include "messageProcessor.h"
#include "communicationService.h"
#include "dofControl.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/
static void messageIdNotRecognizedError();

static void handleManipulatorOnOffMessage(uint8_t * msg);
static void handleChangeDofValuesMessages(
		void (*funToExecute)(uint16_t value,
				dofNumbers_t dofNumber),
		uint16_t value, uint8_t dofNumber );
static void handleDofValueMessage(uint8_t * msg);
static void handleDofIncreaseMessage(uint8_t * msg);
static void handleDofDecreaseMessage(uint8_t * msg);
static void handleGripperMessage(uint8_t * msg);

static void messageReceivingStateMachine(message_TypeDef message);
/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/
static void messageIdNotRecognizedError(){
	// TODO
}

static void handleManipulatorOnOffMessage(uint8_t * msg){

	if( 1 == msg[0] ){
		robotStart();
	} else {
		robotStop();
	}
}

static void handleChangeDofValuesMessages(
		void (*funToExecute)(uint16_t value,
				dofNumbers_t dofNumber),
		uint16_t value, uint8_t dofNumber ){

	switch(dofNumber){
		case dofNumber1:
			funToExecute(value, dofNumber1);
		break;
		case dofNumber2:
			funToExecute(value, dofNumber2);
		break;
		case dofNumber3:
			funToExecute(value, dofNumber3);
		break;
		case dofNumber4:
			funToExecute(value, dofNumber4);
		break;
		case dofNumber5:
			funToExecute(value, dofNumber5);
		break;
		case dofNumber6:
			funToExecute(value, dofNumber6);
		break;
		default:
		break;
	}
}

static void handleDofValueMessage(uint8_t * msg){

	uint16_t angle = msg[1] + msg[2] + msg[3];

	handleChangeDofValuesMessages(setDofAngle, angle, msg[0]);
}

static void handleDofIncreaseMessage(uint8_t * msg){

	handleChangeDofValuesMessages(increaseDofAngle, msg[1], msg[0]);
}

static void handleDofDecreaseMessage(uint8_t * msg){

	handleChangeDofValuesMessages(decreaseDofAngle, msg[1], msg[0]);
}

static void handleGripperMessage(uint8_t * msg){

	if( 1 == msg[0] ){
		openGripper();
	} else {
		closeGripper();
	}
}

static void messageReceivingStateMachine(message_TypeDef message){

	switch(message.ID){
		case MANIPULATOR_ON_OFF_ID:
			handleManipulatorOnOffMessage(message.message);
		break;
		case DOF_VALUE_MSG_ID:
			handleDofValueMessage(message.message);
		break;
		case DOF_INCREASE_MSG_ID:
			handleDofIncreaseMessage(message.message);
		break;
		case DOF_DECREASE_MSG_ID:
			handleDofDecreaseMessage(message.message);
		break;
		case GRIPPER_OPEN_CLOSE_ID:
			handleGripperMessage(message.message);
		break;
		default:
			messageIdNotRecognizedError();
		break;
	}
}
/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/
void processNextMessage(){

	messageReceivingStateMachine( getNextMessage() );
}
/*******************************************************************************
 *    end of file
 ******************************************************************************/


