/*
 * uart.c
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include "communicationService.h"
#include "uart.h"

// drivers to handle uart comunication
#include "stm32l4xx.h"

// other libraries
#include <string.h>
#include <stdbool.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/
static void UART2_Init();
static void UART_StartReceiving();

static uint8_t convertASCIItoINT(uint8_t character);
static uint8_t convertINTtoASCII(uint8_t character);
static bool checkIfFirstCharacterOK(uint8_t character);
static bool checkIfLastCharacterOK(uint8_t character);

static void handleMsgNotStartedState(uint8_t character);
static void handleMsgStartedState(uint8_t character);
static void handleMsgStartedAfterIDState(uint8_t character);
static void handleMessageToEndState(uint8_t character);
static void handleReceivedChar(uint8_t character);

static void UART_InitError();
static void UART_SendingError();
static void UART_ReceivingError();
/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

typedef enum{
	UART_STATE_FREE,
	UART_STATE_BUSY,
}uartStates_t;

typedef enum{
	MSG_NOT_STARTED,
	MSG_STARTED,
	MSG_STARTED_AFTER_ID,
	MSG_TO_END
}msg_state_t;
/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/
// data to handle uart communication
UART_HandleTypeDef huart2;

volatile uint8_t inputData;
volatile static uint8_t outputData[TX_BUFFER_SIZE];

static uint8_t uartState = UART_STATE_FREE;

// receiving messages
msg_state_t receivedMsgState = MSG_NOT_STARTED;
message_TypeDef receivedMessage = {0};
uint8_t messageReceivedDataCounter = 0;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/
/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*
 * helper functions
 */
static inline uint8_t convertASCIItoINT(uint8_t character){
	return character - 48;
}

static inline uint8_t convertINTtoASCII(uint8_t uintValue){
	return uintValue + 48;
}

static inline bool checkIfFirstCharacterOK(uint8_t character){
	return (MESSAGE_START_CHAR == character);
}

static inline bool checkIfLastCharacterOK(uint8_t character){
	return (MESSAGE_END_CHAR == character);
}

/*
 * init functions
 */
static void UART2_Init(){

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

	if( HAL_UART_Init(&huart2) != HAL_OK){
		UART_InitError();
	}
}

static void UART_StartReceiving(){

	if( HAL_OK != HAL_UART_Receive_IT(&huart2, &inputData, 1) ){
		UART_ReceivingError();
	}
}

/*
 * errors handling
 */
static void UART_InitError(){
	// TODO
	while(1);
}

static void UART_SendingError(){
	//TODO
}

static void UART_ReceivingError(){
	// TODO
}


/*
 * message receiving functions
 */
static void handleMsgNotStartedState(uint8_t character){

	if( checkIfFirstCharacterOK(character) ){
		receivedMsgState = MSG_STARTED;
	}
}

static void handleMsgStartedState(uint8_t character){

	receivedMessage.ID = character;
	receivedMsgState = MSG_STARTED_AFTER_ID;

}

static void handleMsgStartedAfterIDState(uint8_t character){

	receivedMessage.message[messageReceivedDataCounter] = character;
	messageReceivedDataCounter++;

	if( messageReceivedDataCounter == MESSAGE_MAX_MESSAGE_SIZE){
		receivedMsgState = MSG_TO_END;
		messageReceivedDataCounter = 0;
	}
}

static void handleMessageToEndState(uint8_t character){

	if( checkIfLastCharacterOK(character) ){
		commPutMessageToTheBuffer(receivedMessage);
	} else{
		receivedMessage.ID = 0;
		receivedMessage.message[0] = 0;
		receivedMessage.message[1] = 0;
		receivedMessage.message[2] = 0;
	}
	receivedMsgState = MSG_NOT_STARTED;
}

static void handleReceivedChar(uint8_t character){

	switch(receivedMsgState){
		case MSG_NOT_STARTED:
			handleMsgNotStartedState(character);
		break;
		case MSG_STARTED:
			handleMsgStartedState(character);
		break;
		case MSG_STARTED_AFTER_ID:
			handleMsgStartedAfterIDState(character);
		break;
		case MSG_TO_END:
			handleMessageToEndState(character);
		break;
	}

	UART_StartReceiving();
}
/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/
// Module Initialization
void uartComInit(void){

	UART2_Init();
	UART_StartReceiving();
}

// Sending messages
void uartSendNextMessage(void){

	if( !isOutputBufferEmpty()){
		if(uartState != UART_STATE_BUSY){

			uartState = UART_STATE_BUSY;

			message_TypeDef messageToSend = commGetNextMessage();

			outputData[MESSAGE_START_CHAR_POSITION] = MESSAGE_START_CHAR;
			outputData[MESSAGE_ID_CHAR_POSITION] = convertINTtoASCII(messageToSend.ID);
			outputData[MESSAGE_STOP_CHAR_POSITION] = MESSAGE_END_CHAR;

			for( uint8_t i = 0; i < MESSAGE_MAX_MESSAGE_SIZE; i++){

				outputData[i + MESSAGE_VALUE_CHARS_POSITION] \
								= messageToSend.message[i];
			}

			if( HAL_OK != HAL_UART_Transmit_IT(&huart2, outputData , TX_BUFFER_SIZE) ){
				UART_SendingError();
			}
		}
	}
}

/*******************************************************************************
 *    HARDWARE CALLBACKS
 ******************************************************************************/

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	handleReceivedChar(inputData);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){

	uartState = UART_STATE_FREE;

	if( ! isOutputBufferEmpty() ){
		uartSendNextMessage();
	}
}
/*******************************************************************************
 *    end of file
 ******************************************************************************/

