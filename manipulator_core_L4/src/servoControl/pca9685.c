/*
 * pca9685.c
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 *
 *  based on library by:
 *		Author: Mateusz Salamon
 *		mateusz@msalamon.pl
 *      Website: https://msalamon.pl/nigdy-wiecej-multipleksowania-na-gpio!-max7219-w-akcji-cz-3/
 *      GitHub:  https://github.com/lamik/Servos_PWM_STM32_HAL
 */

/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "pca9685.h"
#include "i2c.h"
#include "math.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/
static PCA9685_STATUS PCA9685_SetBit(uint8_t Register,
		uint8_t Bit, uint8_t Value);

static PCA9685_STATUS PCA9685_SoftwareReset(void);
static PCA9685_STATUS PCA9685_SleepMode(uint8_t Enable);
static PCA9685_STATUS PCA9685_RestartMode(uint8_t Enable);
static PCA9685_STATUS PCA9685_AutoIncrement(uint8_t Enable);

static PCA9685_STATUS PCA9685_SubaddressRespond(
		SubaddressBit Subaddress, uint8_t Enable);
static PCA9685_STATUS PCA9685_AllCallRespond(uint8_t Enable);

static PCA9685_STATUS PCA9685_SetPwm(uint8_t Channel,
		uint16_t OnTime, uint16_t OffTime);

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/
I2C_HandleTypeDef pca9685_i2c;

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/
static PCA9685_STATUS PCA9685_SetBit(uint8_t Register,
		uint8_t Bit, uint8_t Value){

	uint8_t tmp;
	if(Value) Value = 1;

	if(HAL_OK != HAL_I2C_Mem_Read(&pca9685_i2c, PCA9685_ADDRESS, Register, 1, &tmp, 1, 10))
	{
		return PCA9685_ERROR;
	}
	tmp &= ~((1<<PCA9685_MODE1_RESTART_BIT)|(1<<Bit));
	tmp |= (Value&1)<<Bit;

	if(HAL_OK != HAL_I2C_Mem_Write(&pca9685_i2c, PCA9685_ADDRESS, Register, 1, &tmp, 1, 10))
	{
		return PCA9685_ERROR;
	}

	return PCA9685_OK;
}

static PCA9685_STATUS PCA9685_SoftwareReset(void){
	uint8_t cmd = 0x6;
	if(HAL_OK == HAL_I2C_Master_Transmit(&pca9685_i2c, 0x00, &cmd, 1, 10))
	{
		return PCA9685_OK;
	}
	return PCA9685_ERROR;
}

static PCA9685_STATUS PCA9685_SleepMode(uint8_t Enable){
	return PCA9685_SetBit(PCA9685_MODE1, PCA9685_MODE1_SLEEP_BIT, Enable);
}

static PCA9685_STATUS PCA9685_RestartMode(uint8_t Enable){
	return PCA9685_SetBit(PCA9685_MODE1, PCA9685_MODE1_RESTART_BIT, Enable);
}

static PCA9685_STATUS PCA9685_AutoIncrement(uint8_t Enable){
	return PCA9685_SetBit(PCA9685_MODE1, PCA9685_MODE1_AI_BIT, Enable);
}

static PCA9685_STATUS PCA9685_SubaddressRespond(
		SubaddressBit Subaddress, uint8_t Enable){

	return PCA9685_SetBit(PCA9685_MODE1, Subaddress, Enable);
}

static PCA9685_STATUS PCA9685_AllCallRespond(uint8_t Enable){
	return PCA9685_SetBit(PCA9685_MODE1, PCA9685_MODE1_ALCALL_BIT, Enable);
}

static PCA9685_STATUS PCA9685_SetPwm(uint8_t Channel,
		uint16_t OnTime, uint16_t OffTime){

	uint8_t RegisterAddress;
	uint8_t Message[4];

	RegisterAddress = PCA9685_LED0_ON_L + (4 * Channel);
	Message[0] = OnTime & 0xFF;
	Message[1] = OnTime>>8;
	Message[2] = OffTime & 0xFF;
	Message[3] = OffTime>>8;

	if(HAL_OK != HAL_I2C_Mem_Write(&pca9685_i2c, PCA9685_ADDRESS, RegisterAddress, 1, Message, 4, 10))
	{
		return PCA9685_ERROR;
	}

	return PCA9685_OK;
}
/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

PCA9685_STATUS PCA9685_Init(){

	I2C3_Init(&pca9685_i2c);

	PCA9685_SoftwareReset();
#ifdef PCA9685_SERVO_MODE
	PCA9685_SetPwmFrequency(48);
#else
	PCA9685_SetPwmFrequency(1000);
#endif
	PCA9685_AutoIncrement(1);

	return PCA9685_OK;
}

//	Frequency - Hz value
PCA9685_STATUS PCA9685_SetPwmFrequency(uint16_t Frequency){

	float PrescalerVal;
	uint8_t Prescale;

	if(Frequency >= 1526)
	{
		Prescale = 0x03;
	}
	else if(Frequency <= 24)
	{
		Prescale = 0xFF;
	}
	else
	{
		PrescalerVal = (25000000 / (4096.0 * (float)Frequency)) - 1;
		Prescale = floor(PrescalerVal + 0.5);
	}

	//	To change the frequency, PCA9685 have to be in Sleep mode.
	PCA9685_SleepMode(1);
	HAL_I2C_Mem_Write(&pca9685_i2c, PCA9685_ADDRESS, PCA9685_PRESCALE, 1, &Prescale, 1, 10); // Write Prescale value
	PCA9685_SleepMode(0);
	PCA9685_RestartMode(1);
	return PCA9685_OK;
}

PCA9685_STATUS PCA9685_SetChannelPWM(uint8_t Channel,
		uint16_t PWM_Value, uint8_t Invert){

  if(PWM_Value > 4095) PWM_Value = 4095;

  if (Invert) {
    if (PWM_Value == 0) {
      // Special value for signal fully on.
      return PCA9685_SetPwm(Channel, 4096, 0);
    }
    else if (PWM_Value == 4095) {
      // Special value for signal fully off.
    	return PCA9685_SetPwm(Channel, 0, 4096);
    }
    else {
    	return PCA9685_SetPwm(Channel, 0, 4095-PWM_Value);
    }
  }
  else {
    if (PWM_Value == 4095) {
      // Special value for signal fully on.
    	return PCA9685_SetPwm(Channel, 4096, 0);
    }
    else if (PWM_Value == 0) {
      // Special value for signal fully off.
    	return PCA9685_SetPwm(Channel, 0, 4096);
    }
    else {
    	return PCA9685_SetPwm(Channel, 0, PWM_Value);
    }
  }
}

#ifdef PCA9685_SERVO_MODE
PCA9685_STATUS PCA9685_SetServoAngle(uint8_t Channel,
		float Angle, servoTypes servo){

	float Value;
	switch(servo){
		case KG25_180_SERVO:
			if(Angle < MIN_ANGLE) Angle = MIN_ANGLE;
			if(Angle > MAX_ANGLE) Angle = MAX_ANGLE;

			Value = (Angle - MIN_ANGLE) * ((float)KG25_180_SERVO_MAX - (float)KG25_180_SERVO_MIN) / (MAX_ANGLE - MIN_ANGLE) + (float)KG25_180_SERVO_MIN;

			return PCA9685_SetChannelPWM(Channel, (uint16_t)Value, 0);
		case KG25_270_SERVO:
				if(Angle < MIN_ANGLE) Angle = MIN_ANGLE;
				if(Angle > MAX_ANGLE_270) Angle = MAX_ANGLE_270;

				Value = (Angle - MIN_ANGLE) * ((float)KG25_270_SERVO_MAX - (float)KG25_270_SERVO_MIN) / (MAX_ANGLE_270 - MIN_ANGLE) + (float)KG25_270_SERVO_MIN;

				return PCA9685_SetChannelPWM(Channel, (uint16_t)Value, 0);
		case POWER_HD_1711MG_SERVO:
				if(Angle < MIN_ANGLE) Angle = MIN_ANGLE;
				if(Angle > MAX_ANGLE) Angle = MAX_ANGLE;

				Value = (Angle - MIN_ANGLE) * ((float)POWER_HD_1711MG_SERVO_MAX - (float)POWER_HD_1711MG_SERVO_MIN) / (MAX_ANGLE - MIN_ANGLE) + (float)POWER_HD_1711MG_SERVO_MIN;

				return PCA9685_SetChannelPWM(Channel, (uint16_t)Value, 0);
		case SG90_SERVO:
				if(Angle < MIN_ANGLE) Angle = MIN_ANGLE;
				if(Angle > MAX_ANGLE) Angle = MAX_ANGLE;

				Value = (Angle - MIN_ANGLE) * ((float)SG90_SERVO_MAX - (float)SG90_SERVO_MIN) / (MAX_ANGLE - MIN_ANGLE) + (float)SG90_SERVO_MIN;

				return PCA9685_SetChannelPWM(Channel, (uint16_t)Value, 0);
		case FEETECH_FT3325M_SERVO:
			if(Angle < MIN_ANGLE) Angle = MIN_ANGLE;
			if(Angle > MAX_ANGLE) Angle = MAX_ANGLE_120;

			Value = (Angle - MIN_ANGLE) * ((float)FEETECH_FT3325M_SERVO_MAX - (float)FEETECH_FT3325M_SERVO_MIN) / (MAX_ANGLE_120 - MIN_ANGLE) + (float)FEETECH_FT3325M_SERVO_MIN;

			return PCA9685_SetChannelPWM(Channel, (uint16_t)Value, 0);

		default:
			return PCA9685_ERROR;
	}
}

#endif
/*******************************************************************************
 *    end of file
 ******************************************************************************/

