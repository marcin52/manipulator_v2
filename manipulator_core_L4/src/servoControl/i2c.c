/*
 * i2c.c
 *
 *  Created on: 20.03.2020
 *      Author: marci
 */

#include "main.h"
#include "stm32l4xx.h"
#include "stm32l4xx_nucleo_32.h"

void I2C3_Init(I2C_HandleTypeDef * hi2c3)
{

  hi2c3->Instance = I2C3;
  hi2c3->Init.Timing = 0x00702991;
  hi2c3->Init.OwnAddress1 = 0;
  hi2c3->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3->Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3->Init.OwnAddress2 = 0;
  hi2c3->Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c3->Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3->Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(hi2c3) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter
    */
  if (HAL_I2CEx_ConfigAnalogFilter(hi2c3, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
   // _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter
    */
  if (HAL_I2CEx_ConfigDigitalFilter(hi2c3, 0) != HAL_OK)
  {
   // _Error_Handler(__FILE__, __LINE__);
  }

}
