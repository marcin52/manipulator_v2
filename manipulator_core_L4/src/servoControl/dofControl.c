/*
 * dofControl.c
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "dofControl.h"
#include "pca9685.h"
#include <math.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define DOF_1_SERVO					KG25_180_SERVO
#define DOF_2_SERVO					KG25_270_SERVO
#define DOF_3_SERVO					KG25_270_SERVO
#define DOF_4_SERVO					FEETECH_FT3325M_SERVO
#define DOF_5_SERVO					POWER_HD_1711MG_SERVO
#define DOF_6_SERVO					0 // not used
#define GRIPPER_SERVO				SG90_SERVO

#define DOF1_MAX_ANGLE				180
#define DOF2_MAX_ANGLE				270
#define DOF3_MAX_ANGLE				270
#define DOF4_MAX_ANGLE				180
#define DOF5_MAX_ANGLE				180
#define DOF6_MAX_ANGLE				180
#define GRIPPER_MAX_ANGLE			180

#define DOF1_INIT_ANGLE 			90
#define DOF2_INIT_ANGLE 			135
#define DOF3_INIT_ANGLE 			135
#define DOF4_INIT_ANGLE 			60
#define DOF5_INIT_ANGLE 			90
#define DOF6_INIT_ANGLE 			90

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/
static void angleValueError(dofNumbers_t dofNumber);
static void hardwareError(dofNumbers_t dofNumber);
static void changeAngleValueInHardware(dofNumbers_t dofNumber, float Angle, servoTypes servo);
static void decrementAngle(dofNumbers_t dofNumber);
static void incrementAngle(dofNumbers_t dofNumber);
static void changeDofAngle(uint16_t angle, dofNumbers_t dofNumber);
static void changeGripperState(gripperState_t state);
/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/


// tables to store info about dof servos

static uint16_t dofInitAngles[6] = {DOF1_INIT_ANGLE, DOF2_INIT_ANGLE,
		DOF3_INIT_ANGLE, DOF4_INIT_ANGLE,
		DOF5_INIT_ANGLE, DOF6_INIT_ANGLE
};


static uint16_t dofActualAngles[6] = {DOF1_INIT_ANGLE, DOF2_INIT_ANGLE,
		DOF3_INIT_ANGLE, DOF4_INIT_ANGLE,
		DOF5_INIT_ANGLE, DOF6_INIT_ANGLE
};

static uint16_t dofExpectedAngles[6] = {DOF1_INIT_ANGLE, DOF2_INIT_ANGLE,
		DOF3_INIT_ANGLE, DOF4_INIT_ANGLE,
		DOF5_INIT_ANGLE, DOF6_INIT_ANGLE
};


static uint16_t dofServoTypes[6] = {DOF_1_SERVO, DOF_2_SERVO,
		DOF_3_SERVO, DOF_4_SERVO,
		DOF_5_SERVO, DOF_6_SERVO
};

static uint16_t dofMaxAngles[6] = {DOF1_MAX_ANGLE, DOF2_MAX_ANGLE,
		DOF3_MAX_ANGLE, DOF4_MAX_ANGLE,
		DOF5_MAX_ANGLE, DOF6_MAX_ANGLE
};

static gripperState_t expectedGripperState = GRIPPER_OPEN;
static gripperState_t actualGripperState = GRIPPER_OPEN;

static robotState_t robotState = ROBOT_OFF;
/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/
static void angleValueError(dofNumbers_t dofNumber){
	// TODO
}

static void hardwareError(dofNumbers_t dofNumber){
	// TODO
}

static void changeAngleValueInHardware(dofNumbers_t dofNumber,
		float Angle, servoTypes servo){

	if( PCA9685_OK != PCA9685_SetServoAngle(dofNumber, Angle, servo) ){
		hardwareError(dofNumber);
	}
}

static void decrementAngle(dofNumbers_t dofNumber){

	uint16_t difference = dofActualAngles[dofNumber] - dofExpectedAngles[dofNumber];

	if( 1 == difference ){
		changeDofAngle(dofExpectedAngles[dofNumber], dofNumber);
	} else {
		uint16_t tmpAngle = dofActualAngles[dofNumber] - 2;
		changeDofAngle(tmpAngle, dofNumber);
	}
}

static void incrementAngle(dofNumbers_t dofNumber){

	uint16_t difference = dofExpectedAngles[dofNumber] - dofActualAngles[dofNumber];

	if( 1 == difference ){
		changeDofAngle(dofExpectedAngles[dofNumber], dofNumber);
	} else {
		uint16_t tmpAngle = dofActualAngles[dofNumber] + 2;
		changeDofAngle(tmpAngle, dofNumber);
	}
}

static void changeDofAngle(uint16_t angle, dofNumbers_t dofNumber){

	if( ROBOT_ON == robotState ){

		if( ( angle >= 0 ) && ( angle <= dofMaxAngles[dofNumber] ) ){
			changeAngleValueInHardware(dofNumber, (float)angle,
					dofServoTypes[dofNumber] );
			dofActualAngles[dofNumber] = angle;
		} else {
			angleValueError(dofNumber);
		}
	}
}

static void changeGripperState(gripperState_t state){

	if( GRIPPER_CLOSE == state ){
		changeAngleValueInHardware(gripperDofNumber,
					(float)GRIPPER_MAX_ANGLE, GRIPPER_SERVO);
	} else {
		changeAngleValueInHardware(gripperDofNumber,
					(float)0, GRIPPER_SERVO);
	}

	actualGripperState = state;
}
/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

/*
 * Init
 */
void initDofControl(){

	PCA9685_Init();

	// Initialization Angles
	robotStart();
	for(uint8_t i = 0; i < dofNumber6; i++){
		dofActualAngles[i] = dofInitAngles[i];
		dofExpectedAngles[i] = dofInitAngles[i];
		changeDofAngle( getDofAngle(i), i);
	}
	robotStop();
}

inline void robotStart(){
	robotState = ROBOT_ON;
}

inline void robotStop(){
	robotState = ROBOT_OFF;
}

inline robotState_t getRobotState(){
	return robotState;
}

/*
 * DOF control
 */
void increaseDofAngle(uint16_t value, dofNumbers_t dofNumber){

	if( ROBOT_ON == robotState ){

		uint16_t angle = getDofAngle(dofNumber) + value;

		if( value < dofMaxAngles[dofNumber] ){
			setDofAngle(angle, dofNumber);
		} else {
			setDofAngle(dofMaxAngles[dofNumber], dofNumber);
		}
	}
}

void decreaseDofAngle(uint16_t value, dofNumbers_t dofNumber){

	if( ROBOT_ON == robotState ){

		uint16_t angle = getDofAngle(dofNumber) - value;

		if( value > 0 ){
			setDofAngle(angle, dofNumber);
		} else {
			setDofAngle(0, dofNumber);
		}
	}
}


void setDofAngle(uint16_t angle, dofNumbers_t dofNumber){

	if( ROBOT_ON == robotState ){

		if( angle < dofMaxAngles[dofNumber] ){

			dofExpectedAngles[dofNumber] = angle;
		}
	}

}

inline uint16_t getDofAngle(dofNumbers_t dofNumber){
	return dofActualAngles[dofNumber];
}

inline uint16_t getDofMaxAngle(dofNumbers_t dofNumber){
	 return dofMaxAngles[dofNumber];
}

inline uint16_t getExpectedDofAngle(dofNumbers_t dofNumber){
	return dofExpectedAngles[dofNumber];
}

void processAngleChanging(dofNumbers_t dofNumber){

	if( dofExpectedAngles[dofNumber] > dofActualAngles[dofNumber]){

		incrementAngle(dofNumber);

	} else if( dofExpectedAngles[dofNumber] < dofActualAngles[dofNumber]){

		decrementAngle(dofNumber);

	}
}

/*
 *    Gripper control
 */
inline void closeGripper(){

	expectedGripperState = GRIPPER_CLOSE;
}

inline void openGripper(){

	expectedGripperState = GRIPPER_OPEN;
}

inline void processChangedGripperState(){

	if( expectedGripperState != actualGripperState ){
		changeGripperState(expectedGripperState);
	}
}

inline gripperState_t getGripperState(){

	return actualGripperState;
}
/*******************************************************************************
 *    end of file
 ******************************************************************************/






