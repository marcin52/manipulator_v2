/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32l4xx.h"
#include "stm32l4xx_nucleo_32.h"

#include "core.h"

#include "uart.h"
#include "dofControl.h"
#include "communicationService.h"
#include "messageProcessor.h"

#include "FreeRTOS.h"
#include "task.h"

// task handlers
TaskHandle_t xInputReadTaskHandle = NULL;
TaskHandle_t xSendMessageTaskHandle = NULL;
TaskHandle_t xSendChangeDof1TaskHandle = NULL;
TaskHandle_t xSendChangeDof2TaskHandle = NULL;
TaskHandle_t xSendChangeDof3TaskHandle = NULL;
TaskHandle_t xSendChangeDof4TaskHandle = NULL;
TaskHandle_t xSendChangeDof5TaskHandle = NULL;
TaskHandle_t xSendChangeDof6TaskHandle = NULL;
TaskHandle_t xSendGripperTaskHandle = NULL;

static void configSEGGER();

// tasks
static void vInputRead_Task(void *pvParameters );
static void vSendMessage_Task(void *pvParameters);
static void vChangeDofAngle_Task(void *pvParameters );
static void vGripper_Task(void *pvParameters );

int main(void)
{
	configSEGGER();

	HAL_Init();
	SystemClock_Config();
	initDofControl();
	communicationServiceInit();

	SEGGER_SYSVIEW_Conf();
	SEGGER_SYSVIEW_Start();

	// Tasks creation

	xTaskCreate( vInputRead_Task,"Input-task",configMINIMAL_STACK_SIZE,
			NULL,3,&xInputReadTaskHandle);

	xTaskCreate( vSendMessage_Task,"Output-task",configMINIMAL_STACK_SIZE,
			NULL,1,&xSendMessageTaskHandle);

	xTaskCreate( vChangeDofAngle_Task,"Dof1-task",configMINIMAL_STACK_SIZE,
			NULL,2,&xSendChangeDof1TaskHandle);

	xTaskCreate( vChangeDofAngle_Task,"Dof2-task",configMINIMAL_STACK_SIZE,
			NULL,2,&xSendChangeDof2TaskHandle);

	xTaskCreate( vChangeDofAngle_Task,"Dof3-task",configMINIMAL_STACK_SIZE,
			NULL,2,&xSendChangeDof3TaskHandle);

	xTaskCreate( vChangeDofAngle_Task,"Dof4-task",configMINIMAL_STACK_SIZE,
			NULL,2,&xSendChangeDof4TaskHandle);

	xTaskCreate( vChangeDofAngle_Task,"Dof5-task",configMINIMAL_STACK_SIZE,
			NULL,2,&xSendChangeDof5TaskHandle);

	// currently not used
	//xTaskCreate( vChangeDofAngle_Task,"Dof6-task",configMINIMAL_STACK_SIZE,
	//		NULL,2,&xSendChangeDof6TaskHandle);

	xTaskCreate( vGripper_Task,"Gripper-task",configMINIMAL_STACK_SIZE,
			NULL,4,&xSendGripperTaskHandle);


	vTaskStartScheduler();

	while(1);
}

static void configSEGGER(){
	DWT->CTRL |= ( 1 << 0); // Enable CYCCNT in DWT
}

static void vInputRead_Task(void *pvParameters ){

	while(1){

		// reading received messages
		if( ! isInputBufferEmpty() ){
			processNextMessage();
		}

		vTaskDelay(pdMS_TO_TICKS(2));
	}
}

static void vSendMessage_Task(void *pvParameters){

	while(1){

		// sending messages
		if( ! isOutputBufferEmpty() ){
			uartSendNextMessage();
		}

		vTaskDelay(pdMS_TO_TICKS(5));
	}
}

static void vChangeDofAngle_Task(void *pvParameters ){

	dofNumbers_t dofNumber = *((dofNumbers_t *)pvParameters);

	while(1){

		processAngleChanging(dofNumber);
		vTaskDelay(pdMS_TO_TICKS(50));
	}
}


static void vGripper_Task(void *pvParameters ){

	while(1){

		processChangedGripperState();

		vTaskDelay(pdMS_TO_TICKS(1));
	}
}
