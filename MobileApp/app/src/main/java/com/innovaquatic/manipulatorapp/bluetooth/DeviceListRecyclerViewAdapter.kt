package com.innovaquatic.manipulatorapp.bluetooth

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.innovaquatic.manipulatorapp.R
import kotlinx.android.synthetic.main.view_device_item_list.view.*

class DeviceListRecyclerViewAdapter(private val deviceNames: Array<String?>, private val showAddress: Boolean = true,
                                    private val deviceAddress: Array<String?>? = null) :
    RecyclerView.Adapter<DeviceListRecyclerViewAdapter.MyViewHolder>() {

    interface Listener {
        fun onClick( position: Int)
    }

    class MyViewHolder(val linearLayout: View) : RecyclerView.ViewHolder(linearLayout)

    private var listener: Listener? = null

    fun setListener( listener: Listener){
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DeviceListRecyclerViewAdapter.MyViewHolder {

        val linearLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_device_item_list, parent, false)

        return MyViewHolder(linearLayout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.linearLayout.device_name.text = deviceNames[position]

        if( showAddress ){
            holder.linearLayout.device_address.text = deviceAddress!![position]
        } else {
            holder.linearLayout.device_address.visibility = View.INVISIBLE
        }

        holder.linearLayout.setOnClickListener{
            if ( listener != null){
                listener?.onClick(position)
            }
        }
    }

    override fun getItemCount() = deviceNames.size
}
