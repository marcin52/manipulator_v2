package com.innovaquatic.manipulatorapp

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.ContentValues
import android.content.Intent
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.os.AsyncTask
import android.view.View
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import com.innovaquatic.manipulatorapp.bluetooth.BluetoothSerial
import com.innovaquatic.manipulatorapp.bluetooth.BluetoothSerialListener
import com.innovaquatic.manipulatorapp.bluetooth.DialogBluetoothDeviceList
import java.io.IOException

class BluetoothCommService(
    private val activity: AppCompatActivity,
    private val bluetoothImage: ImageButton
):
    BluetoothSerialListener, DialogBluetoothDeviceList.OnDeviceSelectedListener {

    // Request code for enabling bluetooth activity intent
    private val ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH = 1

    // Bluetooth variables
    private var isBluetoothConnected : Boolean = false
    private lateinit var bluetoothSerial : BluetoothSerial
    private var isBluetoothEnableRequestSended = false
    private var isConnectionDeviceRequested = false

    // Messages IDs
    private val MANIPULATOR_ON_OFF_ID = 1
    private val DOF_VALUE_MSG_ID = 11
    private val DOF_INCREASE_MSG_ID	= 12
    private val DOF_DECREASE_MSG_ID	= 13
    private val GRIPPER_OPEN_CLOSE_ID = 21

    // Message config
    private val messageStartChar = '{'
    private val messageStopChar = '}'

    // dof numbers in robot hardware
    enum class DofNumbers(val number: Int){
        dof1Number(0), dof2Number(1), dof3Number(2),
        dof4Number(3), dof5Number(4), dof6Number(5)
    }

    init {

        bluetoothSerial = BluetoothSerial(activity, this)

        bluetoothImage.setOnClickListener {
            bluetoothImageOnClickListener()
        }
    }

    // Overriding functions
    override fun onBluetoothNotSupported() {
        showToast("Bluetooth is not supported on this device")
    }

    override fun onBluetoothDisabled() {
        if( ! isBluetoothEnableRequestSended) {
            val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBluetooth, ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH)
            isBluetoothEnableRequestSended = true
        }
    }

    override fun onBluetoothDeviceDisconnected() {
        updateBluetoothState()
        if( isConnectionDeviceRequested ){
            showToast("disconnected from device")
            isConnectionDeviceRequested = false
        }
    }

    override fun onConnectingBluetoothDevice() {
        updateBluetoothState()
        showToast("connectiong to the device")
    }

    override fun onBluetoothDeviceConnected(name: String?, address: String?) {
        updateBluetoothState()
        showToast("Connected to $name")
    }

    override fun onBluetoothSerialRead(message: String?) {

    }

    override fun onBluetoothSerialWrite(message: String?) {

    }

    override fun onBluetoothDeviceSelected(device: BluetoothDevice?) {
        // Connect to the selected remote Bluetooth device
        bluetoothSerial.connect(device)
    }

    // Private functions
    private fun bluetoothImageOnClickListener(){

        when (getBluetoothState()) {

            BluetoothSerial.STATE_CONNECTED -> {
                bluetoothSerial.stop()
            }
            BluetoothSerial.STATE_DISCONNECTED -> {

                if (bluetoothSerial.isBluetoothEnabled) {
                    showDeviceListDialog()
                } else {
                    onBluetoothDisabled()
                }
            }
            else -> {
                bluetoothSerial.stop()
                showToast("bluetooth connection stopped")
            }
        }
    }

    private fun showDeviceListDialog() {
        // Display dialog for selecting a remote Bluetooth device
        isConnectionDeviceRequested = true
        val dialog = DialogBluetoothDeviceList(activity)
        dialog.setOnDeviceSelectedListener(this)
        dialog.setDevices(bluetoothSerial.pairedDevices)
        dialog.showAddress(true)
        dialog.show(activity.supportFragmentManager, "bluetooth device picker")
    }

    private fun getBluetoothState() : Int {
        return if (bluetoothSerial != null) bluetoothSerial.state else BluetoothSerial.STATE_DISCONNECTED
    }

    private fun updateBluetoothState(){

        when (getBluetoothState()) {
            BluetoothSerial.STATE_CONNECTING -> {
                isBluetoothConnected = false
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_not_connected_white_100))
            }
            BluetoothSerial.STATE_CONNECTED -> {
                isBluetoothConnected = true
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_connected_blue_100))
            }
            else -> {
                isBluetoothConnected = false
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_not_connected_white_100))
            }
        }
    }

    private fun showToast(text: String){
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show()
    }

    private fun bluetoothSend(msgID: Int, msgArray: Array<Int>) { // msgValue array max size is 3

        var dataToSend = ""
        dataToSend += messageStartChar
        dataToSend += msgID.toChar()

        for (element in msgArray) {
            dataToSend += element.toChar()
        }

        dataToSend += messageStopChar

        BluetoothSendTask(bluetoothSerial).execute(dataToSend)
    }

    private fun sendDofAngle( dofNumbers: DofNumbers, angle: Int){

        var firstValToSend = angle
        var secondValToSend = 0
        var thirdValToSend = 0

        if (firstValToSend > 254) {
            firstValToSend -= 254
            secondValToSend = 127
            thirdValToSend = 127
        }

        if (firstValToSend > 126) {
            firstValToSend -= 127
            secondValToSend = 127
        }

        val arrayToSend = arrayOf(dofNumbers.ordinal, firstValToSend, secondValToSend, thirdValToSend)
        bluetoothSend(DOF_VALUE_MSG_ID, arrayToSend)
    }

    private fun increaseDofAngle(dofNumbers: DofNumbers, value: Int){
        var firstValToSend = value
        var secondValToSend = 0
        var thirdValToSend = 0

        val arrayToSend = arrayOf(dofNumbers.ordinal, firstValToSend, secondValToSend, thirdValToSend)
        bluetoothSend(DOF_INCREASE_MSG_ID, arrayToSend)
    }

    private fun decreaseDofAngle(dofNumbers: DofNumbers, value: Int){
        var firstValToSend = value
        var secondValToSend = 0
        var thirdValToSend = 0

        val arrayToSend = arrayOf(dofNumbers.ordinal, firstValToSend, secondValToSend, thirdValToSend)
        bluetoothSend(DOF_DECREASE_MSG_ID, arrayToSend)
    }

    // Public functions

    fun setupSPPService(){
        bluetoothSerial.setup()
    }

    fun onEnableBluetoothActivityResult(requestCode: Int, resultCode: Int) {

        when (requestCode) {
            ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH ->                 // Set up Bluetooth serial port when Bluetooth adapter is turned on
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    bluetoothSerial.setup()
                }
        }
    }

    fun sendRobotStartStop(startOrStop: Boolean){

        val intValue = if(startOrStop){
            1
        } else {
            0
        }

        val arrayToSend = arrayOf(intValue, 0, 0, 0)
        bluetoothSend(MANIPULATOR_ON_OFF_ID, arrayToSend)
    }

    fun sendGripperState(gripperState: Boolean){

        val intValue = if(gripperState){
            1
        } else {
            0
        }

        val arrayToSend = arrayOf(intValue, 0, 0, 0)
        bluetoothSend(GRIPPER_OPEN_CLOSE_ID, arrayToSend)
    }

    fun sendDof1Value(value: Int){
        sendDofAngle(DofNumbers.dof1Number, value)
    }

    fun sendDof2Value(value: Int){
        sendDofAngle(DofNumbers.dof2Number, value)
    }

    fun sendDof3Value(value: Int){
        sendDofAngle(DofNumbers.dof3Number, value)
    }

    fun sendDof4Value(value: Int){
        sendDofAngle(DofNumbers.dof4Number, value)
    }

    fun sendDof5Value(value: Int){
        sendDofAngle(DofNumbers.dof5Number, value)
    }

    fun sendDof6Value(value: Int){
        sendDofAngle(DofNumbers.dof6Number, value)
    }

    fun increaseDof1Angle(value: Int){
        increaseDofAngle(DofNumbers.dof1Number, value)
    }

    fun increaseDof2Angle(value: Int){
        increaseDofAngle(DofNumbers.dof2Number, value)
    }

    fun increaseDof3Angle(value: Int){
        increaseDofAngle(DofNumbers.dof3Number, value)
    }

    fun increaseDof4Angle(value: Int){
        increaseDofAngle(DofNumbers.dof4Number, value)
    }

    fun increaseDof5Angle(value: Int){
        increaseDofAngle(DofNumbers.dof5Number, value)
    }

    fun increaseDof6Angle(value: Int){
        increaseDofAngle(DofNumbers.dof6Number, value)
    }

    fun decreaseDof1Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof1Number, value)
    }

    fun decreaseDof2Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof2Number, value)
    }

    fun decreaseDof3Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof3Number, value)
    }

    fun decreaseDof4Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof4Number, value)
    }

    fun decreaseDof5Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof5Number, value)
    }

    fun decreaseDof6Angle(value: Int){
        decreaseDofAngle(DofNumbers.dof6Number, value)
    }

    // Class to send bluetooth messages in non-blocking way
    private class BluetoothSendTask(private val bluetoothSerial : BluetoothSerial) : AsyncTask<String?, Void?, Boolean>() {

        override fun doInBackground(vararg p0: String?): Boolean {

            return try {
                bluetoothSerial.write(p0[0])
                true
            } catch (e: IOException){
                false
            }
        }
    }
}