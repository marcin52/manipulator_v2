package com.innovaquatic.manipulatorapp.bluetooth

import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.innovaquatic.manipulatorapp.R
import kotlinx.android.synthetic.main.dialog_devicelistitem.*

class DialogBluetoothDeviceList(private var mContext: Context?): DialogFragment(),
                    DeviceListRecyclerViewAdapter.Listener{

    interface OnDeviceSelectedListener {

        fun onBluetoothDeviceSelected(device: BluetoothDevice?)
    }

    private var mListener: OnDeviceSelectedListener? = null
    private var mDevices: Set<BluetoothDevice>? = null
    private lateinit var mNames: Array<String?>
    private lateinit var mAddresses: Array<String?>
    private var mShowAddress = true

    fun setOnDeviceSelectedListener(listener: OnDeviceSelectedListener?) {
        mListener = listener
    }

    fun setDevices(devices: Set<BluetoothDevice>) {
        mDevices = devices
        if (devices != null) {
            mNames = arrayOfNulls(devices.size)
            mAddresses = arrayOfNulls(devices.size)
            for ((i, d) in devices.withIndex()) {
                mNames[i] = d.name
                mAddresses[i] = d.address
            }
        }
    }

    fun showAddress(showAddress: Boolean) {
        mShowAddress = showAddress
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = context?.let { MaterialAlertDialogBuilder(it)}

        builder!!.setView(R.layout.dialog_devicelistitem)

        val dialog = builder.create()
        dialog.show()

        // Recycler view settings
        val adapter = DeviceListRecyclerViewAdapter(mNames, mShowAddress, mAddresses)
        adapter.setListener(this)
        dialog.device_recycler_view.setHasFixedSize(true)
        dialog.device_recycler_view.layoutManager = LinearLayoutManager(context)
        dialog.device_recycler_view.adapter = adapter

        return dialog
    }

    override fun onClick(position: Int) {
        mListener!!.onBluetoothDeviceSelected(BluetoothSerial.getAdapter(mContext).getRemoteDevice(
            mAddresses[position]))
        dialog!!.cancel()
    }


}