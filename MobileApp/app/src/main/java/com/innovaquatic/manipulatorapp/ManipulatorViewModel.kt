package com.innovaquatic.manipulatorapp

import androidx.lifecycle.ViewModel

class ManipulatorViewModel: ViewModel() {

    var isRobotOn: Boolean = false

    var isGripperOn: Boolean = false

    var dof1Angle: Int = 90
    var dof2Angle: Int = 135
    var dof3Angle: Int = 135
    var dof4Angle: Int = 90
    var dof5Angle: Int = 90
    var dof6Angle: Int = 90
}
