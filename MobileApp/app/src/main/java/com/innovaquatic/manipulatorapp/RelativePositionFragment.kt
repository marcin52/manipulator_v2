package com.innovaquatic.manipulatorapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.innovaquatic.manipulatorapp.customviews.ViewRelativeAngle
import com.innovaquatic.manipulatorapp.databinding.FragmentAbsolutePositionBinding
import com.innovaquatic.manipulatorapp.databinding.FragmentRelativePositionBinding

class RelativePositionFragment : Fragment(), ViewRelativeAngle.OnAngleChangeListener {

    private lateinit var binding: FragmentRelativePositionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRelativePositionBinding.inflate(inflater, container, false)

        binding.dof1.setOnAngleChangedListener(this)
        binding.dof2.setOnAngleChangedListener(this)
        binding.dof3.setOnAngleChangedListener(this)
        binding.dof4.setOnAngleChangedListener(this)
        binding.dof5.setOnAngleChangedListener(this)
        binding.dof6.setOnAngleChangedListener(this)



        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.dof1.actualizeAngleText((activity as MainActivity).mViewModel.dof1Angle)
        binding.dof2.actualizeAngleText((activity as MainActivity).mViewModel.dof2Angle)
        binding.dof3.actualizeAngleText((activity as MainActivity).mViewModel.dof3Angle)
        binding.dof4.actualizeAngleText((activity as MainActivity).mViewModel.dof4Angle)
        binding.dof5.actualizeAngleText((activity as MainActivity).mViewModel.dof5Angle)
        binding.dof6.actualizeAngleText((activity as MainActivity).mViewModel.dof6Angle)
    }

    override fun onAngleDecreased(view: View, value: Int) {

        when(view){
            binding.dof1 ->{
                (activity as MainActivity).commService.decreaseDof1Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof1Angle - value
                (activity as MainActivity).mViewModel.dof1Angle = angle
                binding.dof1.actualizeAngleText(angle)
            }
            binding.dof2 ->{
                (activity as MainActivity).commService.decreaseDof2Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof2Angle - value
                (activity as MainActivity).mViewModel.dof2Angle = angle
                binding.dof2.actualizeAngleText(angle)
            }
            binding.dof3 ->{
                (activity as MainActivity).commService.decreaseDof3Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof3Angle - value
                (activity as MainActivity).mViewModel.dof3Angle = angle
                binding.dof3.actualizeAngleText(angle)
            }
            binding.dof4 ->{
                (activity as MainActivity).commService.decreaseDof4Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof4Angle - value
                (activity as MainActivity).mViewModel.dof4Angle = angle
                binding.dof4.actualizeAngleText(angle)
            }
            binding.dof5 ->{
                (activity as MainActivity).commService.decreaseDof5Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof5Angle - value
                (activity as MainActivity).mViewModel.dof5Angle = angle
                binding.dof5.actualizeAngleText(angle)
            }
            binding.dof6 ->{
                (activity as MainActivity).commService.decreaseDof6Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof6Angle - value
                (activity as MainActivity).mViewModel.dof6Angle = angle
                binding.dof6.actualizeAngleText(angle)
            }
        }
    }

    override fun onAngleIncreased(view: View, value: Int) {

        when(view){
            binding.dof1 ->{
                (activity as MainActivity).commService.increaseDof1Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof1Angle + value
                (activity as MainActivity).mViewModel.dof1Angle = angle
                binding.dof1.actualizeAngleText(angle)
            }
            binding.dof2 ->{
                (activity as MainActivity).commService.increaseDof2Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof2Angle + value
                (activity as MainActivity).mViewModel.dof2Angle = angle
                binding.dof2.actualizeAngleText(angle)
            }
            binding.dof3 ->{
                (activity as MainActivity).commService.increaseDof3Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof3Angle + value
                (activity as MainActivity).mViewModel.dof3Angle = angle
                binding.dof3.actualizeAngleText(angle)
            }
            binding.dof4 ->{
                (activity as MainActivity).commService.increaseDof4Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof4Angle + value
                (activity as MainActivity).mViewModel.dof4Angle = angle
                binding.dof4.actualizeAngleText(angle)
            }
            binding.dof5 ->{
                (activity as MainActivity).commService.increaseDof5Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof5Angle + value
                (activity as MainActivity).mViewModel.dof5Angle = angle
                binding.dof5.actualizeAngleText(angle)
            }
            binding.dof6 ->{
                (activity as MainActivity).commService.increaseDof6Angle(value)
                val angle =  (activity as MainActivity).mViewModel.dof6Angle + value
                (activity as MainActivity).mViewModel.dof6Angle = angle
                binding.dof6.actualizeAngleText(angle)
            }
        }
    }


}