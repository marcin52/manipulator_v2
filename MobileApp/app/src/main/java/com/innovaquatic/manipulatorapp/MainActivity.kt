package com.innovaquatic.manipulatorapp

import android.content.Intent
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.innovaquatic.manipulatorapp.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var commService: BluetoothCommService

    lateinit var mViewModel: ManipulatorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mViewModel = ViewModelProviders.of(this).get(ManipulatorViewModel::class.java)
        commService = BluetoothCommService(this, binding.bluetoothButton)

        restoreScreenData()

        binding.linearUp.robot_start_button.setOnClickListener{
            robotStartButtonOnClick()
        }

        binding.linearUp.gripperSwitch.setOnCheckedChangeListener{ _, b ->
            gripperSwitchCheckedChanged(b)
        }

        val pageAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val pager: ViewPager = findViewById(R.id.contentFrame)
        pager.adapter = pageAdapter

        val tabLayout: TabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(pager)
    }

    override fun onStart() {
        super.onStart()
        commService.setupSPPService()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        commService.onEnableBluetoothActivityResult(requestCode, resultCode)
    }

    private fun robotStartButtonOnClick() {
        if( mViewModel.isRobotOn ){
            commService.sendRobotStartStop(false)
            binding.robotStartButton.setText("ROBOT_OFF")
            mViewModel.isRobotOn = false
        } else {
            commService.sendRobotStartStop(true)
            binding.robotStartButton.setText("ROBOT_ON")
            mViewModel.isRobotOn = true
        }
    }

    private fun gripperSwitchCheckedChanged( isChecked: Boolean){
        mViewModel.isGripperOn = isChecked
        commService.sendGripperState(mViewModel.isGripperOn)

        if(isChecked){
            binding.linearUp.gripperText.text = "Gripper Open"
        } else {
            binding.linearUp.gripperText.text = "Gripper Close"
        }
    }

    private fun restoreScreenData(){

        binding.gripperSwitch.isChecked = mViewModel.isGripperOn

    }
}