package com.innovaquatic.manipulatorapp.customviews

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import com.innovaquatic.manipulatorapp.R

class ViewRelativeAngle constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
)
    : LinearLayout(context, attrs, defStyleAttr),
    View.OnClickListener {

    constructor(context: Context?) : this(context!!) {}

    constructor(context: Context?, attrs: AttributeSet?) : this(
        context!!, attrs
    ) {}

    companion object {
        private inline val Int.dp2Px
            get() = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                this.toFloat(),
                Resources.getSystem().displayMetrics)

        private inline val Float.sp2Px
            get() = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                this,
                Resources.getSystem().displayMetrics)
    }

    interface OnAngleChangeListener {
        fun onAngleDecreased(view: View, value: Int)
        fun onAngleIncreased(view: View, value: Int)
    }

    private lateinit var onAngleChangeListener: OnAngleChangeListener

    private var startProgress = 90
    private var maxProgress = 180

    private lateinit var buttonMinus5: Button
    private lateinit var buttonMinus2: Button
    private lateinit var buttonMinus1: Button
    private lateinit var buttonPlus5: Button
    private lateinit var buttonPlus2: Button
    private lateinit var buttonPlus1: Button
    private lateinit var text: TextView


    init {

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AbsoluteAngleView,
            0, 0
        ).apply {

            try {

                startProgress = getInteger(
                    R.styleable.AbsoluteAngleView_startingProgress, 90
                )
                maxProgress = getInteger(
                    R.styleable.AbsoluteAngleView_maxProgress, 180
                )


            } finally {
                recycle()
            }
        }

        setOrientation(HORIZONTAL)

        initLeftButtons()
        initTextView()
        initRightButtons()

    }

    private fun initTextView() {

        text = TextView(context)

        val params = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        params.setMargins(16.dp2Px.toInt(), 0, 16.dp2Px.toInt(), 0)
        params.gravity = Gravity.CENTER

        text.layoutParams = params

        text.text = startProgress.toString()
        text.setTextAppearance(android.R.style.TextAppearance_DeviceDefault_Medium)

        addView(text)
    }

    private fun initButton( text: String, marginLeft: Int, marginRight: Int ) : Button{

        val button = Button(context)

        val params = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT,
            1f
        )
        params.setMargins(marginLeft, 0, marginRight, 0)
        params.gravity = Gravity.CENTER

        button.layoutParams = params

        button.text = text

        return button
    }

    private fun initLeftButtons(){

        buttonMinus5 = initButton("-5", 16.dp2Px.toInt(), 0)
        buttonMinus5.setOnClickListener(this)
        addView(buttonMinus5)
        buttonMinus2 = initButton("-2", 16.dp2Px.toInt(), 0)
        buttonMinus2.setOnClickListener(this)
        addView(buttonMinus2)
        buttonMinus1 = initButton("-1", 16.dp2Px.toInt(), 0)
        buttonMinus1.setOnClickListener(this)
        addView(buttonMinus1)
    }

    private fun initRightButtons(){
        buttonPlus1 = initButton("+1", 0, 16.dp2Px.toInt())
        buttonPlus1.setOnClickListener(this)
        addView(buttonPlus1)
        buttonPlus2 = initButton("+2", 0, 16.dp2Px.toInt())
        buttonPlus2.setOnClickListener(this)
        addView(buttonPlus2)
        buttonPlus5 = initButton("+5", 0, 16.dp2Px.toInt())
        buttonPlus5.setOnClickListener(this)
        addView(buttonPlus5)
    }

    override fun onClick(p0: View?) {

        when(p0){
            buttonMinus5 -> angleDecreased(5)
            buttonMinus2 -> angleDecreased(2)
            buttonMinus1 -> angleDecreased(1)
            buttonPlus1 -> angleIncreased( 1)
            buttonPlus2 -> angleIncreased( 2)
            buttonPlus5 -> angleIncreased( 5)
        }
    }

    private fun angleDecreased(value: Int){

        onAngleChangeListener.onAngleDecreased(this, value)
    }

    private fun angleIncreased(value: Int){
        onAngleChangeListener.onAngleIncreased(this, value)
    }

    fun setOnAngleChangedListener(listener: OnAngleChangeListener) {
        onAngleChangeListener = listener
    }

    fun actualizeAngleText( value: Int ){
        text.text = value.toString()
    }

}