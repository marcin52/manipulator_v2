package com.innovaquatic.manipulatorapp.customviews

import android.R.attr
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.innovaquatic.manipulatorapp.R


class ViewAbsoluteAngle constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
)
    : LinearLayout(context, attrs, defStyleAttr),
    SeekBar.OnSeekBarChangeListener{

    constructor(context: Context?) : this(context!!) { }

    constructor(context: Context?, attrs: AttributeSet?) : this(
        context!!, attrs
    ) { }

    interface OnProgressChangeListener{
        fun onProgressChange( view: View, value: Int )
    }



    private lateinit var onProgressChangeListener: OnProgressChangeListener

    private var startProgress = 90
    private var maxProgress = 180

    private lateinit var seekbar: SeekBar
    private lateinit var text: TextView


    init {

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AbsoluteAngleView,
            0, 0
        ).apply {

            try {

                startProgress = getInteger(
                    R.styleable.AbsoluteAngleView_startingProgress, 90
                )
                maxProgress = getInteger(
                    R.styleable.AbsoluteAngleView_maxProgress, 180
                )


            } finally {
                recycle()
            }
        }

        setOrientation(HORIZONTAL)

        initSeekbar()
        initTextView()

    }

    private fun initSeekbar(){

        seekbar = SeekBar(context)

        val params = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT,
            1f
        )
        params.setMargins(20,30,20, 0)
        seekbar.layoutParams = params

        seekbar.max = maxProgress
        seekbar.progress = startProgress

        seekbar.setOnSeekBarChangeListener(this)

        addView(seekbar)

    }

    private fun initTextView(){

        text = TextView(context)

        val params = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        params.setMargins(0,30,20, 0)
        text.layoutParams = params

        text.text =  startProgress.toString()

        addView(text)
    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
        onProgressChangeListener.onProgressChange(this, p1)
        text.text = p1.toString()
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {}

    override fun onStopTrackingTouch(p0: SeekBar?) {}

    fun setOnProgressChangedListener(listener: OnProgressChangeListener){
        onProgressChangeListener = listener
    }

    fun actualizeProgress(value: Int){
        seekbar.progress = value
    }

}