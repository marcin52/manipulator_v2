package com.innovaquatic.manipulatorapp

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.innovaquatic.manipulatorapp.customviews.ViewAbsoluteAngle
import com.innovaquatic.manipulatorapp.databinding.FragmentAbsolutePositionBinding

class AbsolutePositionFragment : Fragment(), ViewAbsoluteAngle.OnProgressChangeListener {

    private lateinit var binding: FragmentAbsolutePositionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentAbsolutePositionBinding.inflate(inflater, container, false)

        binding.dof1.setOnProgressChangedListener(this)
        binding.dof2.setOnProgressChangedListener(this)
        binding.dof3.setOnProgressChangedListener(this)
        binding.dof4.setOnProgressChangedListener(this)
        binding.dof5.setOnProgressChangedListener(this)
        binding.dof6.setOnProgressChangedListener(this)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.dof1.actualizeProgress((activity as MainActivity).mViewModel.dof1Angle)
        binding.dof2.actualizeProgress((activity as MainActivity).mViewModel.dof2Angle)
        binding.dof3.actualizeProgress((activity as MainActivity).mViewModel.dof3Angle)
        binding.dof4.actualizeProgress((activity as MainActivity).mViewModel.dof4Angle)
        binding.dof5.actualizeProgress((activity as MainActivity).mViewModel.dof5Angle)
        binding.dof6.actualizeProgress((activity as MainActivity).mViewModel.dof6Angle)
    }

    override fun onProgressChange(view: View, value: Int) {

        when(view){
            binding.dof1 -> {
                (activity as MainActivity).commService.sendDof1Value(value)
                (activity as MainActivity).mViewModel.dof1Angle = value
            }
            binding.dof2 -> {
                (activity as MainActivity).commService.sendDof2Value(value)
                (activity as MainActivity).mViewModel.dof2Angle = value
            }
            binding.dof3 -> {
                (activity as MainActivity).commService.sendDof3Value(value)
                (activity as MainActivity).mViewModel.dof3Angle = value
            }
            binding.dof4 -> {
                (activity as MainActivity).commService.sendDof4Value(value)
                (activity as MainActivity).mViewModel.dof4Angle = value
            }
            binding.dof5 -> {
                (activity as MainActivity).commService.sendDof5Value(value)
                (activity as MainActivity).mViewModel.dof5Angle = value
            }
            binding.dof6 -> {
                (activity as MainActivity).commService.sendDof6Value(value)
                (activity as MainActivity).mViewModel.dof6Angle = value
            }
        }
    }

}