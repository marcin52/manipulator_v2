package com.innovaquatic.manipulatorapp

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class SectionsPagerAdapter(
    private val mContext: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {

        var fragment: Fragment?
        when (position) {
            0 -> fragment = RelativePositionFragment()
            1 -> fragment = AbsolutePositionFragment()
            2 -> fragment = InverseKinematicsFragment()
            else -> {

                fragment = AbsolutePositionFragment()
            }
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {

        when (position) {
            0 -> return mContext.resources.getString(R.string.relativeTab)
            1 -> return mContext.resources.getString(R.string.absoluteTab)
            2 -> return mContext.resources.getString(R.string.inverseKinematicsTab)

            else -> {
                return null
            }
        }
    }
}
